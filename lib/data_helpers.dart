import 'package:flutter_svg/svg.dart';

import 'constants.dart';

SvgPicture getNowWeatherIcon(String code, int cloudinessPercentage, [double size = weatherIconSize]) {
  if (['01d', '02d', '03d', '04d'].any((it) => code == it)) {
    if (cloudinessPercentage <= 10) {
      return SvgPicture.asset(assetNameIconSun, width: size, height: size);
    } else if (cloudinessPercentage <= 50) {
      return SvgPicture.asset(assetNameIconLightCloudSun, width: size, height: size);
    } else if (cloudinessPercentage <= 95) {
      return SvgPicture.asset(assetNameIconHeavyCloudSun, width: size, height: size);
    } else {
      return SvgPicture.asset(assetNameIconCloud, width: size, height: size);
    }
  } else if (['01n', '02n', '03n', '04n'].any((it) => code == it)) {
    if (cloudinessPercentage <= 10) {
      return SvgPicture.asset(assetNameIconMoonAndStars, width: size, height: size);
    } else if (cloudinessPercentage <= 50) {
      return SvgPicture.asset(assetNameIconLightCloudMoon, width: size, height: size);
    } else if (cloudinessPercentage <= 95) {
      return SvgPicture.asset(assetNameIconHeavyCloudMoon, width: size, height: size);
    } else {
      return SvgPicture.asset(assetNameIconCloud, width: size, height: size);
    }
  } else if (code == '09d' || code == '09n') {
    return SvgPicture.asset(assetNameIconShowers, width: size, height: size);
  } else if (code == '10d') {
    return SvgPicture.asset(assetNameIconCloudSunRain, width: size, height: size);
  } else if (code == '10n') {
    return SvgPicture.asset(assetNameIconCloudMoonRain, width: size, height: size);
  } else if (code == '11d' || code == '11n') {
    return SvgPicture.asset(assetNameIconThunderstorm, width: size, height: size);
  } else if (code == '13d' || code == '13n') {
    return SvgPicture.asset(assetNameIconSnowflake, width: size, height: size);
  } else {
    return SvgPicture.asset(assetNameIconCloud, width: size, height: size);
  }
}

SvgPicture getWeatherIcon(String code, int cloudinessPercentage, int chanceOfPrecip, [double size = weatherIconSize]) {
  if (code == '11d' || code == '11n') {
    return SvgPicture.asset(assetNameIconThunderstorm, width: size, height: size);
  } else if (code == '13d' || code == '13n') {
    return SvgPicture.asset(assetNameIconSnowflake, width: size, height: size);
  } else if (code.startsWith('09')) {
    if (chanceOfPrecip >= 40) {
      return SvgPicture.asset(assetNameIconShowers, width: size, height: size);
    } else {
      return getNowWeatherIcon('04${code[code.length - 1]}', cloudinessPercentage, size);
    }
  } else if (code == '10d') {
    if (chanceOfPrecip >= 40) {
      return SvgPicture.asset(assetNameIconCloudSunRain, width: size, height: size);
    } else {
      return getNowWeatherIcon('03d', cloudinessPercentage, size);
    }
  } else if (code == '10n') {
    if (chanceOfPrecip >= 40) {
      return SvgPicture.asset(assetNameIconCloudMoonRain, width: size, height: size);
    } else {
      return getNowWeatherIcon('03n', cloudinessPercentage, size);
    }
  } else if (['01d', '02d', '03d', '04d'].any((it) => code == it)) {
    if (chanceOfPrecip >= 40) {
      if (cloudinessPercentage >= 95 || chanceOfPrecip >= 50) {
        return SvgPicture.asset(assetNameIconShowers, width: size, height: size);
      } else {
        return SvgPicture.asset(assetNameIconCloudSunRain, width: size, height: size);
      }
    } else {
      return getNowWeatherIcon(code, cloudinessPercentage, size);
    }
  } else if (['01n', '02n', '03n', '04n'].any((it) => code == it)) {
    if (chanceOfPrecip >= 40) {
      if (cloudinessPercentage >= 95 || chanceOfPrecip >= 50) {
        return SvgPicture.asset(assetNameIconShowers, width: size, height: size);
      } else {
        return SvgPicture.asset(assetNameIconCloudMoonRain, width: size, height: size);
      }
    } else {
      return getNowWeatherIcon(code, cloudinessPercentage, size);
    }
  } else {
    return SvgPicture.asset(assetNameIconCloud, width: size, height: size);
  }
}

String getTime(DateTime dateTime, bool is24HourClock) {
  final hour = is24HourClock ? dateTime.hour : dateTime.hour % 12;
  final minute = dateTime.minute.toString().padLeft(2, '0');
  return '$hour:$minute';
}

String getShortCityName(String longCityName) {
  String shortCityName = longCityName;
  if (shortCityName.contains(countrySep)) {
    shortCityName = shortCityName.split(countrySep).first;
  }
  if (shortCityName.contains(', ')) {
    shortCityName = shortCityName.split(', ').first;
  }
  return getSafeTitle(shortCityName);
}

getSafeTitle(String title) {
  if (title == null) {
    return appTitle;
  } else if (title.startsWith(prefixNear)) {
    return title.replaceFirst(prefixNear, '');
  } else {
    return title;
  }
}
