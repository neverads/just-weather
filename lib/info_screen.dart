import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:myapp/constants.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InfoScreenState();
  }
}

class InfoScreenState extends State<InfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(infoScreenTitle),
      ),
      body: ListView(
        padding: EdgeInsets.all(32),
        primary: false,
        children: [
          whatItDoSection,
          thatsItSection,
          whatsTheCatchSection,
          dataSourceSection,
          Platform.isAndroid ? isThereAWidgetSection : Container(),
        ],
      ),
    );
  }

  Column get whatItDoSection {
    return infoSection(
      header: 'What does this app do?',
      body: '''We show you the current weather, as well as hourly & daily forecasts.  That's it.''',
    );
  }

  Column get thatsItSection {
    return infoSection(
      header: '''That's it?''',
      body: 'Yup!  No one likes bloated apps, so we decided to keep ours minimalist.  '
          'That means that the most important info is in plain sight, '
          'and the app takes up very little storage space.\n\n'
          'If you have a request for a new feature or something we should add, email us at "never.ads.info@gmail.com".',
    );
  }

  Column get whatsTheCatchSection {
    return infoSection(
      header: '''What's the catch?''',
      body: '''No catch!  We survive on donations, good reviews, and word of mouth.\n\n'''
          '''We obviously don't make money from ads or selling users' data - and the app is free.  '''
          '''We dream of one day having enough income from donations to quit our jobs, but that's a long way off… ''',
    );
  }

  Column get dataSourceSection {
    return infoSection(
      header: 'Where do we get our data?',
      body: '''Because this app is free & open-source, our forecast data is provided for free by Open Weather.\n\n'''
          '''We hope you don't consider this an "ad."  '''
          '''Some users have asked where we get our data from, so we thought we would mention it here.''',
      shouldIncludeOpenWeatherLink: true,
    );
  }

  Column get isThereAWidgetSection {
    return infoSection(
      header: 'Is there a "widget?"',
      body: '''Indeed!  You can add a widget to your device's home screen so you can see the current weather '''
          '''without needing to open the app.\n\n'''
          '''It doesn't always do its background-updates as it should, and we're really sorry about that.  '''
          '''Use that little refresh button to make sure it's up-to-date.  '''
          '''(Sorry, but we're just amateurs after all.)\n\n'''
          '''How to add a widget can vary, so we suggest you look it up for your specific device/OS.''',
    );
  }

  Column infoSection({
    @required String header,
    @required String body,
    bool shouldIncludeOpenWeatherLink = false,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          header,
          style: Theme.of(context).textTheme.headline5.copyWith(fontWeight: FontWeight.w500),
        ),
        shouldIncludeOpenWeatherLink ? openWeatherLink : SizedBox(height: 16),
        Text(
          body,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        SizedBox(height: 48),
      ],
    );
  }

  Widget get openWeatherLink {
    return Align(
      alignment: Alignment.centerLeft,
      child: FlatButton(
        padding: EdgeInsets.all(0),
        child: SizedBox(
          width: 128,
          child: Image.asset(
            Theme.of(context).brightness == Brightness.light
                ? 'assets/open_weather_logo_light.png'
                : 'assets/open_weather_logo_dark.png',
          ),
        ),
        onPressed: () async {
          final url = 'https://openweathermap.org/';
          if (await canLaunch(url)) {
            await launch(url);
          }
        },
      ),
    );
  }
}
