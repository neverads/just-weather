import 'dart:io';

import 'package:flutter/material.dart';
import 'package:myapp/constants.dart';
import 'package:myapp/main_screen.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(JustWeather());
}

class JustWeather extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final blue = Colors.blue.shade600;
    final blueGrey = Colors.blueGrey.shade600;
    final textColorLightTheme = Colors.blueGrey.shade700;

    final lightBlue = Colors.blue.shade200;
    final lightBlueGrey = Colors.blueGrey.shade50;

    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: blue,
        primaryColorLight: Colors.blueGrey.shade200,
        primaryColorDark: blueGrey,
        accentColor: blue,
        dividerColor: blueGrey,
        cursorColor: blue,
        errorColor: Colors.deepOrange,
        fontFamily: 'Futura',
        textTheme: TextTheme(
          headline3: TextStyle(fontSize: 28, fontWeight: FontWeight.normal, color: textColorLightTheme),
          headline4: TextStyle(fontSize: 22, fontWeight: FontWeight.normal, color: blue),
          headline5: TextStyle(fontSize: 22, fontWeight: FontWeight.normal, color: textColorLightTheme),
          headline6: TextStyle(fontSize: 18, fontWeight: FontWeight.normal, color: textColorLightTheme),
          subtitle1: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: textColorLightTheme, height: 1.7),
          bodyText1: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: blue),
          bodyText2: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: textColorLightTheme),
          caption: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: textColorLightTheme),
        ),
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: blueGrey,
        primaryColorLight: blueGrey,
        primaryColorDark: lightBlueGrey,
        accentColor: lightBlue,
        dividerColor: lightBlueGrey,
        cursorColor: lightBlue,
        errorColor: Colors.deepOrange.shade300,
        fontFamily: 'Futura',
        textTheme: TextTheme(
          headline3: TextStyle(fontSize: 28, fontWeight: FontWeight.normal, color: lightBlueGrey),
          headline4: TextStyle(fontSize: 22, fontWeight: FontWeight.normal, color: lightBlue),
          headline5: TextStyle(fontSize: 22, fontWeight: FontWeight.normal, color: lightBlueGrey),
          headline6: TextStyle(fontSize: 18, fontWeight: FontWeight.normal, color: lightBlueGrey),
          subtitle1: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: lightBlueGrey, height: 1.7),
          bodyText1: TextStyle(fontSize: 16, fontWeight: FontWeight.normal, color: lightBlue),
          bodyText2: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: lightBlueGrey),
          caption: TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: lightBlueGrey),
        ),
      ),
      home: MainScreen(),
    );
  }
}
