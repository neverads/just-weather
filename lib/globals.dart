import 'dart:math';

import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'alert.dart';
import 'constants.dart';

SharedPreferences prefs;

List<String> searchedIds;

Location location;

bool hasConnection = true;
bool wasUseLocationSelected = false;
int whenLocationUsed;
List<Alert> alerts = [];

class WeatherInfo {
  final int dateTime;
  final int temperatureCurrentCelsius;
  final int temperatureCurrentFahrenheit;
  final int temperatureFeelsLikeCelsius;
  final int temperatureFeelsLikeFahrenheit;
  final int temperatureHighCelsius;
  final int temperatureHighFahrenheit;
  final int temperatureLowCelsius;
  final int temperatureLowFahrenheit;
  final int humidityPercentage;
  final String windSpeedKPH;
  final String windGustKPH;
  final String windDirectionFrom;
  final int windDirectionAdjustmentDegrees;
  final int cloudinessPercentage;
  final int weatherId;
  final String weatherTitle;
  final String weatherDescription;
  final String weatherIconCode;
  final DateTime dateTimeUtc;
  final DateTime dateTimeLocal;
  final String cityName;
  final DateTime sunriseLocal;
  final DateTime sunsetLocal;
  final int chanceOfPrecip;

  WeatherInfo({
    this.dateTime,
    this.temperatureCurrentCelsius,
    this.temperatureCurrentFahrenheit,
    this.temperatureFeelsLikeCelsius,
    this.temperatureFeelsLikeFahrenheit,
    this.temperatureHighCelsius,
    this.temperatureHighFahrenheit,
    this.temperatureLowCelsius,
    this.temperatureLowFahrenheit,
    this.humidityPercentage,
    this.windSpeedKPH,
    this.windGustKPH,
    this.windDirectionFrom,
    this.windDirectionAdjustmentDegrees,
    this.cloudinessPercentage,
    this.weatherId,
    this.weatherTitle,
    this.weatherDescription,
    this.weatherIconCode,
    this.dateTimeUtc,
    this.dateTimeLocal,
    this.cityName,
    this.sunriseLocal,
    this.sunsetLocal,
    this.chanceOfPrecip,
  });

  factory WeatherInfo.fromJson(Map<String, dynamic> json, int timezoneOffsetFromApi) {
    const kelvinCelsiusDifference = 273.15;
    const multiplierMetersPerSecondToKilometersPerHour = 3.6;
    final weather = json['weather'][0];

    var _temperatureCurrentCelsius;
    var _temperatureCurrentFahrenheit;
    var _temperatureFeelsLikeCelsius;
    var _temperatureFeelsLikeFahrenheit;
    var _temperatureHighCelsius;
    var _temperatureHighFahrenheit;
    var _temperatureLowCelsius;
    var _temperatureLowFahrenheit;
    final temp = json['temp'];
    if (temp is double || temp is int) {
      _temperatureCurrentCelsius = (temp - kelvinCelsiusDifference).round();
      _temperatureCurrentFahrenheit = celsiusToFahrenheit(_temperatureCurrentCelsius);
      _temperatureFeelsLikeCelsius = (json['feels_like'] - kelvinCelsiusDifference).round();
      _temperatureFeelsLikeFahrenheit = celsiusToFahrenheit(_temperatureFeelsLikeCelsius);
    } else {
      _temperatureHighCelsius = (temp['max'] - kelvinCelsiusDifference).round();
      _temperatureHighFahrenheit = celsiusToFahrenheit(_temperatureHighCelsius);
      _temperatureLowCelsius = (temp['min'] - kelvinCelsiusDifference).round();
      _temperatureLowFahrenheit = celsiusToFahrenheit(_temperatureLowCelsius);
    }
    final num _windSpeedMetersPerSecond = json['wind_speed'];
    final num _windGustMetersPerSecond = json['wind_gust'] == null ? null : json['wind_gust'];
    final _windDeg = json['wind_deg'] ?? 0;

    final now = DateTime.now();
    final deviceToUtcDiffSeconds = (now.toUtc().hour - now.hour) * 3600;
    final timeZoneAdjustment = deviceToUtcDiffSeconds + timezoneOffsetFromApi;
    final _sunriseLocal = json['sunrise'] == null
        ? null
        : DateTime.fromMillisecondsSinceEpoch((json['sunrise'] + timeZoneAdjustment) * 1000);
    final _sunsetLocal = json['sunset'] == null
        ? null
        : DateTime.fromMillisecondsSinceEpoch((json['sunset'] + timeZoneAdjustment) * 1000);
    final double _chanceOfPrecip = json['pop']?.toDouble();

    return WeatherInfo(
      dateTime: json['dt'],
      temperatureCurrentCelsius: _temperatureCurrentCelsius,
      temperatureCurrentFahrenheit: _temperatureCurrentFahrenheit,
      temperatureFeelsLikeCelsius: _temperatureFeelsLikeCelsius,
      temperatureFeelsLikeFahrenheit: _temperatureFeelsLikeFahrenheit,
      temperatureHighCelsius: _temperatureHighCelsius,
      temperatureHighFahrenheit: _temperatureHighFahrenheit,
      temperatureLowCelsius: _temperatureLowCelsius,
      temperatureLowFahrenheit: _temperatureLowFahrenheit,
      humidityPercentage: json['humidity'],
      windSpeedKPH: (_windSpeedMetersPerSecond * multiplierMetersPerSecondToKilometersPerHour).round().toString(),
      windGustKPH: _windGustMetersPerSecond == null
          ? null
          : (_windGustMetersPerSecond * multiplierMetersPerSecondToKilometersPerHour).round().toString(),
      windDirectionFrom: getWindDirectionFromAsString(_windDeg ?? 0),
      windDirectionAdjustmentDegrees: getSimpleWindDirectionFrom(_windDeg),
      cloudinessPercentage: json['clouds'],
      weatherId: weather['id'],
      weatherTitle: weather['main'],
      weatherDescription: weather['description'],
      weatherIconCode: weather['icon'],
      dateTimeUtc: DateTime.fromMillisecondsSinceEpoch(json['dt'] * 1000),
      dateTimeLocal: DateTime.fromMillisecondsSinceEpoch((json['dt'] + timeZoneAdjustment) * 1000),
      cityName: null,
      sunriseLocal: _sunriseLocal,
      sunsetLocal: _sunsetLocal,
      chanceOfPrecip: _chanceOfPrecip == null ? 0 : (_chanceOfPrecip * 100).round(),
    );
  }

  factory WeatherInfo.fromString(String string) {
    final List<String> split = string.split(sep);
    final _null = 'null';
    final _temperatureCurrentCelsius = split[0] == _null ? null : int.parse(split[0]);
    final _temperatureCurrentFahrenheit = split[1] == _null ? null : int.parse(split[1]);
    final _temperatureFeelsLikeCelsius = split[2] == _null ? null : int.parse(split[2]);
    final _temperatureFeelsLikeFahrenheit = split[3] == _null ? null : int.parse(split[3]);
    final _temperatureHighCelsius = split[4] == _null ? null : int.parse(split[4]);
    final _temperatureHighFahrenheit = split[5] == _null ? null : int.parse(split[5]);
    final _temperatureLowCelsius = split[6] == _null ? null : int.parse(split[6]);
    final _temperatureLowFahrenheit = split[7] == _null ? null : int.parse(split[7]);
    final _humidityPercentage = split[8] == _null ? null : int.parse(split[8]);
    final _windSpeed = split[9] == _null ? null : split[9];
    final _windGust = split[10] == _null ? null : split[10];
    final _windDirectionFrom = split[11] == _null ? null : split[11];
    final _windDirectionAdjustmentDegrees = split[12] == _null ? null : int.parse(split[12]);
    final _cloudinessPercentage = split[13] == _null ? null : int.parse(split[13]);
    final _weatherIconCode = split[14] == _null ? null : split[14];
    final _cityName = split[15] == _null ? null : split[15];
    final _dateTimeLocal = split[16] == _null ? null : DateTime.fromMillisecondsSinceEpoch(int.parse(split[16]));
    final _weatherDescription = split[17] == _null ? null : split[17];
    final _sunriseLocal = split[18] == _null ? null : DateTime.fromMillisecondsSinceEpoch(int.parse(split[18]));
    final _sunsetLocal = split[19] == _null ? null : DateTime.fromMillisecondsSinceEpoch(int.parse(split[19]));
    final _chanceOfPrecip = split.length < 21 || split[20] == _null ? null : int.parse(split[20]);
    final _weatherId = split.length < 22 || split[21] == _null ? -1 : int.parse(split[21]);
    return WeatherInfo(
      temperatureCurrentCelsius: _temperatureCurrentCelsius,
      temperatureCurrentFahrenheit: _temperatureCurrentFahrenheit,
      temperatureFeelsLikeCelsius: _temperatureFeelsLikeCelsius,
      temperatureFeelsLikeFahrenheit: _temperatureFeelsLikeFahrenheit,
      temperatureHighCelsius: _temperatureHighCelsius,
      temperatureHighFahrenheit: _temperatureHighFahrenheit,
      temperatureLowCelsius: _temperatureLowCelsius,
      temperatureLowFahrenheit: _temperatureLowFahrenheit,
      humidityPercentage: _humidityPercentage,
      windSpeedKPH: _windSpeed,
      windGustKPH: _windGust,
      windDirectionFrom: _windDirectionFrom,
      windDirectionAdjustmentDegrees: _windDirectionAdjustmentDegrees,
      cloudinessPercentage: _cloudinessPercentage,
      weatherDescription: _weatherDescription,
      weatherIconCode: _weatherIconCode,
      cityName: _cityName,
      dateTimeLocal: _dateTimeLocal,
      sunriseLocal: _sunriseLocal,
      sunsetLocal: _sunsetLocal,
      chanceOfPrecip: _chanceOfPrecip,
      weatherId: _weatherId,
    );
  }

  String toStringForPreferences() {
    return [
      temperatureCurrentCelsius.toString(),
      temperatureCurrentFahrenheit.toString(),
      temperatureFeelsLikeCelsius.toString(),
      temperatureFeelsLikeFahrenheit.toString(),
      temperatureHighCelsius.toString(),
      temperatureHighFahrenheit.toString(),
      temperatureLowCelsius.toString(),
      temperatureLowFahrenheit.toString(),
      humidityPercentage.toString(),
      windSpeedKPH,
      windGustKPH,
      windDirectionFrom,
      windDirectionAdjustmentDegrees.toString(),
      cloudinessPercentage.toString(),
      weatherIconCode,
      cityName,
      dateTimeLocal.millisecondsSinceEpoch.toString(),
      weatherDescription.toString(),
      sunriseLocal?.millisecondsSinceEpoch.toString(),
      sunsetLocal?.millisecondsSinceEpoch.toString(),
      chanceOfPrecip.toString(),
      weatherId.toString(),
    ].join(sep);
  }
}

class DailyForecast {
  List<DailyForecastDataPoint> dataPoints = [];
  List<String> asStringsForPreferences = [];

  DailyForecast(List<WeatherInfo> weatherInfo) {
    weatherInfo.forEach((it) {
      dataPoints.add(DailyForecastDataPoint(
        it.dateTimeLocal.weekday,
        it.weatherIconCode,
        it.temperatureHighCelsius,
        it.temperatureLowCelsius,
        it.cloudinessPercentage,
        it.chanceOfPrecip,
        it.weatherId,
        it.weatherDescription,
      ));
    });
    asStringsForPreferences = weatherInfo.map((it) => it.toStringForPreferences()).toList();
  }

  factory DailyForecast.fromStrings(List<String> strings) {
    return DailyForecast(strings.map((_string) => WeatherInfo.fromString(_string)).toList());
  }
}

class HourlyForecast {
  List<HourlyForecastDataPoint> dataPoints = [];
  List<String> asStringsForPreferences = [];

  HourlyForecast(List<WeatherInfo> weatherInfo) {
    weatherInfo.forEach((it) {
      dataPoints.add(HourlyForecastDataPoint(it));
    });
    asStringsForPreferences = weatherInfo.map((it) => it.toStringForPreferences()).toList();
  }

  int get maxTemp {
    List<int> temps = [];
    dataPoints.forEach((dataPoint) => temps.add(dataPoint.temperatureCelsius));
    final _maxTemp = temps.reduce(max);
    return _maxTemp;
  }

  int get minTemp {
    List<int> temps = [];
    dataPoints.forEach((dataPoint) => temps.add(dataPoint.temperatureCelsius));
    final _minTemp = temps.reduce(min);
    return _minTemp;
  }

  Iterable<int> _tempsForTheNextFewHours(bool isCelsius) {
    return dataPoints
        .sublist(0, hoursToConsiderForHighAndLowTemps)
        .map((dataPoint) => isCelsius ? dataPoint.temperatureCelsius : dataPoint.temperatureFahrenheit);
  }

  int todaysHighTemp(bool isCelsius) {
    return _tempsForTheNextFewHours(isCelsius).reduce(max);
  }

  int todaysLowTemp(bool isCelsius) {
    return _tempsForTheNextFewHours(isCelsius).reduce(min);
  }

  factory HourlyForecast.fromStrings(List<String> strings) {
    return HourlyForecast(strings.map((_string) => WeatherInfo.fromString(_string)).toList());
  }
}

class HourlyForecastDataPoint {
  int hour;
  int day;
  String weatherIconCode;
  int temperatureCelsius;
  int temperatureFahrenheit;
  int cloudinessPercentage;
  int chanceOfPrecip;
  String asStringForPreferences;
  String weatherDescription;
  int weatherId;

  HourlyForecastDataPoint(WeatherInfo dataPoint) {
    hour = dataPoint.dateTimeLocal.hour;
    day = dataPoint.dateTimeLocal.day;
    weatherIconCode = dataPoint.weatherIconCode;
    temperatureCelsius = dataPoint.temperatureCurrentCelsius;
    temperatureFahrenheit = dataPoint.temperatureCurrentFahrenheit;
    cloudinessPercentage = dataPoint.cloudinessPercentage;
    chanceOfPrecip = dataPoint.chanceOfPrecip;
    asStringForPreferences = dataPoint.toStringForPreferences();
    weatherDescription = dataPoint.weatherDescription;
    weatherId = dataPoint.weatherId;
  }
}

class DailyForecastDataPoint {
  String weekday;
  String weekdayFull;
  String weatherIconCode;
  int highCelsius;
  int highFahrenheit;
  int lowCelsius;
  int lowFahrenheit;
  int cloudinessPercentage;
  int chanceOfPrecip;
  int weatherId;
  String weatherDescription;

  DailyForecastDataPoint(
    int weekday,
    String weatherIconCode,
    int highCelsius,
    int lowCelsius,
    int cloudinessPercentage,
    int chanceOfPrecip,
    int weatherId,
    String weatherDescription,
  ) {
    this.weekday = getWeekdayAsString(weekday);
    this.weekdayFull = getFullWeekdayAsString(weekday);
    this.weatherIconCode = weatherIconCode;
    this.highCelsius = highCelsius;
    this.highFahrenheit = celsiusToFahrenheit(highCelsius);
    this.lowCelsius = lowCelsius;
    this.lowFahrenheit = celsiusToFahrenheit(lowCelsius);
    this.cloudinessPercentage = cloudinessPercentage;
    this.chanceOfPrecip = chanceOfPrecip;
    this.weatherId = weatherId;
    this.weatherDescription = weatherDescription;
  }
}

class City {
  String name;
  int id;
  String country;
  double lat;
  double long;

  City(this.name, this.id, this.country, this.lat, this.long);

  factory City.fromIdAsString(String idAsString) {
    final split = idAsString.split(sep);
    final lat = double.parse(split.first);
    final long = double.parse(split[1]);
    final nameAndCountry = split.last.split(countrySep);
    final name = nameAndCountry.first;
    final country = nameAndCountry.last;
    return City(name, 0, country, lat, long);
  }

  String getIdAsString() {
    return '$lat$sep$long$sep$name$countrySep$country';
  }

  bool getIsFromLocationServices() {
    return name.startsWith(prefixNear);
  }
}

class Cities {
  List<City> cities;
  List<String> names;

  Cities(this.cities) {
    names = cities.map((city) => city.name).toList();
    names.sort();
  }
}

String getWindDirectionFromAsString(int degrees) {
  if (degrees < 23 || degrees >= 338) {
    return 'north';
  } else if (degrees < 68) {
    return 'northeast';
  } else if (degrees < 113) {
    return 'east';
  } else if (degrees < 158) {
    return 'southeast';
  } else if (degrees < 203) {
    return 'south';
  } else if (degrees < 248) {
    return 'southwest';
  } else if (degrees < 293) {
    return 'west';
  } else {
    return 'northwest';
  }
}

int getSimpleWindDirectionFrom(int degrees) {
  if (degrees < 23 || degrees >= 338) {
    return 0;
  } else if (degrees < 68) {
    return 45;
  } else if (degrees < 113) {
    return 90;
  } else if (degrees < 158) {
    return 135;
  } else if (degrees < 203) {
    return 180;
  } else if (degrees < 248) {
    return 225;
  } else if (degrees < 293) {
    return 270;
  } else {
    return 315;
  }
}

String getWeekdayAsString(int weekday) {
  if (weekday == 1) {
    return "Mon";
  } else if (weekday == 2) {
    return "Tue";
  } else if (weekday == 3) {
    return "Wed";
  } else if (weekday == 4) {
    return "Thu";
  } else if (weekday == 5) {
    return "Fri";
  } else if (weekday == 6) {
    return "Sat";
  } else if (weekday == 7) {
    return "Sun";
  } else {
    return null;
  }
}

String getFullWeekdayAsString(int weekday) {
  if (weekday == 1) {
    return "Monday";
  } else if (weekday == 2) {
    return "Tueday";
  } else if (weekday == 3) {
    return "Wednesday";
  } else if (weekday == 4) {
    return "Thursday";
  } else if (weekday == 5) {
    return "Friday";
  } else if (weekday == 6) {
    return "Saturday";
  } else if (weekday == 7) {
    return "Sunday";
  } else {
    return null;
  }
}

int celsiusToFahrenheit(int degreesCelsius) {
  return (degreesCelsius * 9 / 5 + 32).round();
}

Future<bool> isLocationEnabled() async {
  if (location == null) {
    location = Location();
    location.changeSettings(accuracy: locationAccuracy);
  }
  return await location.serviceEnabled();
}

Future<bool> hasLocationPermission() async {
  if (location == null) {
    location = Location();
    location.changeSettings(accuracy: locationAccuracy);
  }
  final _permissionStatus = await location.hasPermission();
  if (_permissionStatus == PermissionStatus.denied) {
    return await location.requestPermission() == PermissionStatus.granted;
  } else {
    return _permissionStatus == PermissionStatus.granted;
  }
}

Future<String> getLocationIdAsString() async {
  if (location == null) {
    location = Location();
    location.changeSettings(accuracy: locationAccuracy);
  }
  final locationData = await location.getLocation();
  final lat = locationData.latitude;
  final long = locationData.longitude;

  final addresses = await Geocoder.local.findAddressesFromCoordinates(Coordinates(lat, long));

  Map<String, int> localities = Map();
  Map<String, int> subLocalities = Map();
  String locality;
  String subLocality;
  final fallback = addresses.first.postalCode ??
      addresses.first.subAdminArea ??
      addresses.first.adminArea ??
      addresses.first.countryName ??
      addresses.first.addressLine;

  addresses.reversed.forEach((address) {
    if (locality == null && address.locality != null) {
      locality = address.locality;
    }
    if (subLocality == null && address.subLocality != null) {
      subLocality = address.subLocality;
    }
    if (address.locality != null) {
      int localitiesCount = localities['${address.locality}'] ?? 0;
      localities['${address.locality}'] = localitiesCount + 1;
    }
    if (address.subLocality != null) {
      int subLocalitiesCount = subLocalities['${address.subLocality}'] ?? 0;
      subLocalities['${address.subLocality}'] = subLocalitiesCount + 1;
    }
  });

  if (localities.length > 1) {
    final highestCount = localities.values.reduce(max);
    var countOfLocalitiesWithHighestCount = 0;
    localities.values.forEach((_count) {
      if (_count == highestCount) {
        countOfLocalitiesWithHighestCount++;
      }
    });

    if (countOfLocalitiesWithHighestCount == 1) {
      var isPopularEnough = true;
      localities.values.forEach((_count) {
        if (_count != highestCount) {
          final isSignificantlyMorePopular = highestCount ~/ 2 >= _count;
          isPopularEnough = isPopularEnough && isSignificantlyMorePopular;
        }
      });
      if (isPopularEnough) {
        locality = localities.entries.firstWhere((loc) => loc.value == highestCount).key;
      }
    }
  }

  return '$lat$sep$long$sep$prefixNear${subLocality ?? locality ?? fallback}';
}
