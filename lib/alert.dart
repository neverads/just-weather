class Alert {
  String location;
  String title;
  int startSecondsSinceEpoch;
  int endSecondsSinceEpoch;
  String description;

  Alert(this.location, this.title, this.startSecondsSinceEpoch, this.endSecondsSinceEpoch, this.description);

  factory Alert.fromJson(Map json) {
    return Alert(json['sender_name'], json['event'], json['start'], json['end'], json['description']);
  }
}
