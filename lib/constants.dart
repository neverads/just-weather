import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:location/location.dart';

const appTitle = 'Just Weather';
const infoScreenTitle = 'App Info';

const assetNameCityData = 'assets/current_city_list.json';

const assetNameAppIcon = 'assets/app_icon.png';
const assetNameIconSun = 'assets/sun.svg';
const assetNameIconMoonAndStars = 'assets/moon_and_stars.svg';
const assetNameIconLightCloudSun = 'assets/light_cloud_sun.svg';
const assetNameIconLightCloudMoon = 'assets/light_cloud_moon.svg';
const assetNameIconHeavyCloudSun = 'assets/heavy_cloud_sun.svg';
const assetNameIconHeavyCloudMoon = 'assets/heavy_cloud_moon.svg';
const assetNameIconShowers = 'assets/showers.svg';
const assetNameIconCloudSunRain = 'assets/cloud_sun_rain.svg';
const assetNameIconCloudMoonRain = 'assets/cloud_moon_rain.svg';
const assetNameIconThunderstorm = 'assets/thunderstorm.svg';
const assetNameIconSnowflake = 'assets/snowflake.svg';
const assetNameIconCloud = 'assets/cloud.svg';

const assetNameIconHumidity = 'assets/humidity.svg';
const assetNameIconWind = 'assets/wind.svg';
const assetNameIconSunrise = 'assets/sunrise.svg';
const assetNameIconSunset = 'assets/sunset.svg';

const prefsKeySearchedIds = 'prefsKeySearchedIds';
const prefsKeyIsCelsius = 'prefsKeyIsCelsius';
const prefsKeyIs24HourClock = 'prefsKeyIs24HourClock';
const prefsKeyIsFirstLaunch = 'prefsKeyIsFirstLaunch';
const prefsKeyTimestampsOfCitySelections = 'prefsKeyTimestampsOfCitySelections';
const prefsKeyWasUseLocationSelected = 'prefsKeyWasUseLocationSelected';
const prefsKeyWhenLocationUsed = 'prefsKeyWhenLocationUsed';
const prefsKeyShouldDisplayNowDetails = 'prefsKeyShouldDisplayNowDetails';
const prefsKeyShouldDisplayNowDetailHumidity = 'prefsKeyShouldDisplayNowDetailHumidity';
const prefsKeyShouldDisplayNowDetailWind = 'prefsKeyShouldDisplayNowDetailWind';
const prefsKeyShouldDisplayNowDetailCloudiness = 'prefsKeyShouldDisplayNowDetailCloudiness';
const prefsKeyShouldDisplayNowDetailSunrise = 'prefsKeyShouldDisplayNowDetailSunrise';
const prefsKeyShouldDisplayNowDetailSunset = 'prefsKeyShouldDisplayNowDetailSunset';
const prefsKeyShouldDisplayAlternateButtonMessage = 'prefsKeyShouldDisplayAlternateButtonMessage';
const prefsKeyFetchesCount = 'prefsKeyFetchesCount';

const sep = '___';
const countrySep = ',,,';
const prefixNear = 'Near ';
const urlJustWeatherRepo = 'gitlab.com/neverads/just-weather';

const messageNoDataCollection = "We don't collect your data.";
const messageEnableLocation = 'Please enable your location or select a city.  ($messageNoDataCollection)';
const messageGrantPermission =
    'The app needs permission to access your location.  Please adjust your settings.  ($messageNoDataCollection)';

const textColorAppBarPrimary = Colors.white;
const textColorAppBarSecondary = Colors.white70;

const textStyleAppBarTitle = TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: textColorAppBarPrimary);
const textStyleAppBarPrimary = TextStyle(fontSize: 19, fontWeight: FontWeight.bold, color: textColorAppBarPrimary);
const textStyleAppBarSecondary = TextStyle(fontSize: 13, color: Colors.white70);

const weatherIconHourlyBoxSize = 48.0;
const weatherIconDailyBoxSize = 36.0;
const weatherIconSize = 28.0;
const weatherIconPadding = 10.0;
const mainScreenHorizontalPadding = 24.0;

const locationAccuracy = LocationAccuracy.high;

const hoursToConsiderForHighAndLowTemps = 18;
