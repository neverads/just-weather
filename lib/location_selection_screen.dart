import 'dart:convert';
import 'dart:math' as math;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geocoder/geocoder.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:url_launcher/url_launcher.dart';

import 'constants.dart';
import 'country_code_parser.dart';
import 'data_helpers.dart';
import 'globals.dart';

class LocationSelectionScreen extends StatefulWidget {
  final bool shouldDisplayMessage;

  LocationSelectionScreen(this.shouldDisplayMessage);

  @override
  State<StatefulWidget> createState() {
    return LocationSelectionScreenState();
  }
}

class LocationSelectionScreenState extends State<LocationSelectionScreen> {
  bool isExplanationVisible = false;
  final typeAheadController = TextEditingController();
  List<String> removedWeatherData;
  Cities cities;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location Selection', style: textStyleAppBarTitle),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(32),
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(right: 4),
              child: Row(
                children: [
                  Expanded(
                    child: UseCurrentLocationRow(),
                  ),
                  IconButton(
                    icon: Icon(Icons.help_outline, color: Theme.of(context).primaryColorDark),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Row(
                              children: <Widget>[
                                Padding(
                                  padding: EdgeInsets.only(bottom: 4),
                                  child: Icon(Icons.location_on, color: Theme.of(context).accentColor),
                                ),
                                SizedBox(width: 12),
                                Text('Using your location', style: Theme.of(context).textTheme.headline4),
                              ],
                            ),
                            content: Text(
                              'You only need to turn on your location for a moment.  '
                              'Your device will remember it, so you don\'t have to keep it on.\n\n'
                              '(You can delete locations from your device any time you want, and we don\'t collect your data.)',
                            ),
                            actions: [
                              FlatButton(
                                child: Text('ok'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
            FutureBuilder(
                future: processCityJson(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 4),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Padding(
                                  padding: EdgeInsets.only(right: 16),
                                  child: TypeAheadField(
                                    textFieldConfiguration: TextFieldConfiguration(
                                      decoration: InputDecoration(
                                        prefixIcon: Icon(Icons.location_city, color: Theme.of(context).accentColor),
                                        labelText: ' Select a city',
                                        labelStyle: Theme.of(context).textTheme.headline4,
                                      ),
                                      controller: typeAheadController,
                                    ),
                                    suggestionsCallback: (enteredText) async {
                                      if (enteredText.length < 3 ||
                                          cities == null ||
                                          ['', null].contains(enteredText.trim())) {
                                        return null;
                                      } else {
                                        try {
                                          final citiesStartingWith = cities.cities
                                              .where((city) =>
                                                  city.name.toLowerCase().startsWith(enteredText.toLowerCase()))
                                              .toList();
                                          if (citiesStartingWith.length <= 50) {
                                            citiesStartingWith.sort(
                                                (a, b) => a.name.split(',').first.compareTo(b.name.split(',').first));
                                          }
                                          final otherCitiesContaining = cities.cities
                                              .where((city) =>
                                                  city.name.toLowerCase().indexOf(enteredText.toLowerCase(), 1) > 0)
                                              .toList();
                                          if (otherCitiesContaining.length <= 20) {
                                            otherCitiesContaining.sort((a, b) => a.name.compareTo(b.name));
                                          }
                                          final relevantCities =
                                              (citiesStartingWith + otherCitiesContaining).toSet().toList();
                                          if (relevantCities.length > 0) {
                                            return relevantCities;
                                          } else {
                                            List<Address> addresses = [];
                                            List<City> citiesFromGeolocator = [];
                                            try {
                                              addresses = await Geocoder.local.findAddressesFromQuery(enteredText);
                                            } catch (e) {
                                              return [];
                                            }
                                            addresses.forEach((address) {
                                              if (address.featureName != null &&
                                                  double.tryParse(address.featureName) == null) {
                                                try {
                                                  citiesFromGeolocator.add(
                                                    City(
                                                      '${address.featureName}, ${address.adminArea}',
                                                      -1,
                                                      address.countryName,
                                                      address.coordinates.latitude,
                                                      address.coordinates.longitude,
                                                    ),
                                                  );
                                                } catch (e) {}
                                              }
                                            });
                                            return citiesFromGeolocator;
                                          }
                                        } catch (e) {
                                          return [];
                                        }
                                      }
                                    },
                                    itemBuilder: (context, suggestion) {
                                      return ListTile(
                                        title: Text(suggestion.name),
                                        subtitle: Text(suggestion.country),
                                      );
                                    },
                                    onSuggestionSelected: (suggestion) {
                                      if (isAbleToSelectCity()) {
                                        wasUseLocationSelected = false;
                                        selectLocation(suggestion.getIdAsString(), context);
                                      } else {
                                        showNotAllowedAlert();
                                      }
                                    },
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.help_outline, color: Theme.of(context).primaryColorDark),
                                padding: EdgeInsets.only(top: 6),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Row(
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.only(bottom: 4),
                                              child: Icon(Icons.location_city, color: Theme.of(context).accentColor),
                                            ),
                                            SizedBox(width: 12),
                                            Text('Select a city', style: Theme.of(context).textTheme.headline4),
                                          ],
                                        ),
                                        content: Text(
                                          'We know our list of cities is far from perfect.  If we need to change something, just let us know!\n\n'
                                          'Send us an email, and we\'ll fix it ASAP.  Please include:\n\n'
                                          '1. The city/state/country which needs to be changed (preferably a screenshot).\n'
                                          '2. What you want us to change it to.\n'
                                          '3. Why we should change it.\n\n'
                                          'never.ads.info@gmail.com',
                                        ),
                                        actions: [
                                          FlatButton(
                                            child: Text('copy email'),
                                            onPressed: () {
                                              Clipboard.setData(ClipboardData(text: 'never.ads.info@gmail.com'));
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 24),
                        Visibility(
                          visible: searchedIds.isNotEmpty,
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(16, 24, 16, 8),
                            child: Text("Previous locations:", style: Theme.of(context).textTheme.headline5),
                          ),
                        ),
                        ListView.separated(
                          itemCount: searchedIds.length,
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            if (cities == null) {
                              return null;
                            }
                            final id = searchedIds[index];
                            City city;
                            final split = id.split(sep);
                            Text subtitle;
                            if (split.last.startsWith(prefixNear)) {
                              final name = split.last;
                              final id = 0;
                              final country = '';
                              final lat = double.parse(split.first);
                              final long = double.parse(split[1]);
                              city = City(name, id, country, lat, long);
                            } else {
                              city = cities.cities.firstWhere((_city) => _city.getIdAsString() == id, orElse: () {
                                final nameSplit = split.last.split(countrySep);
                                final name = nameSplit.first;
                                final id = 0;
                                final country = nameSplit.last;
                                final lat = double.parse(split.first);
                                final long = double.parse(split[1]);
                                subtitle = Text(
                                  country,
                                  style: Theme.of(context).textTheme.bodyText2,
                                );
                                return City(name, id, country, lat, long);
                              });
                              subtitle = Text(
                                city.country,
                                style: Theme.of(context).textTheme.bodyText2,
                              );
                            }
                            return ListTile(
                              title: Text(
                                getSafeTitle(city.name),
                                style: Theme.of(context).textTheme.subtitle1,
                              ),
                              subtitle: subtitle,
                              leading: Padding(
                                padding: EdgeInsets.all(8),
                                child: Icon(
                                  city.getIsFromLocationServices() ? Icons.location_on : Icons.location_city,
                                  color: Theme.of(context).primaryColorDark,
                                ),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.clear),
                                color: Theme.of(context).errorColor,
                                onPressed: () {
                                  removedWeatherData = prefs.getStringList(id);
                                  prefs.remove(id);
                                  searchedIds.remove(id);
                                  prefs.setStringList(prefsKeySearchedIds, searchedIds);
                                  setState(() {});
                                  ScaffoldMessenger.of(context).removeCurrentSnackBar();
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text('"${city.name}" deleted.'),
                                      action: SnackBarAction(
                                        label: "Undo",
                                        textColor: Theme.of(context).primaryColorLight,
                                        onPressed: () {
                                          searchedIds.insert(index, id);
                                          prefs.setStringList(prefsKeySearchedIds, searchedIds);
                                          prefs.setStringList(id, removedWeatherData);
                                          setState(() {});
                                        },
                                      ),
                                    ),
                                  );
                                },
                              ),
                              onTap: () {
                                if (isAbleToSelectCity()) {
                                  wasUseLocationSelected = false;
                                  selectLocation(id, context);
                                } else {
                                  showNotAllowedAlert();
                                }
                              },
                            );
                          },
                          separatorBuilder: (context, index) {
                            return Divider(indent: 64, endIndent: 64);
                          },
                        ),
                      ],
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                }),
            SizedBox(height: 32),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CupertinoButton(
                  child: Text(
                    messageNoDataCollection,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  onPressed: () {
                    setState(() {
                      isExplanationVisible = !isExplanationVisible;
                    });
                  },
                ),
                Transform.rotate(
                  angle: isExplanationVisible ? math.pi : 0,
                  child: IconButton(
                    icon: Icon(Icons.arrow_drop_down, color: Theme.of(context).primaryColorDark),
                    onPressed: () {
                      setState(() {
                        isExplanationVisible = !isExplanationVisible;
                      });
                    },
                  ),
                )
              ],
            ),
            Visibility(
              visible: isExplanationVisible,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 16),
                  Text(
                    "We have no interest in knowing which cities you look up.  We don't collect your data, and we never will."
                    "\n\n"
                    "For your convenience your device will remember past searches.  This data doesn't get sent anywhere, and you can delete it anytime."
                    "\n\n"
                    "Your privacy matters to us, as does your trust.  That's why our code is open-source:",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                  Row(
                    children: <Widget>[
                      CupertinoButton(
                        child: Text(urlJustWeatherRepo, style: Theme.of(context).textTheme.bodyText1),
                        padding: EdgeInsets.symmetric(vertical: 16),
                        onPressed: () {
                          launchOpenSourceWebpage();
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 32),
                ],
              ),
            ),
            Visibility(
              visible: widget.shouldDisplayMessage,
              child: Padding(
                padding: EdgeInsets.only(top: 32),
                child: MessageCanTapLocationName(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> processCityJson() async {
    return DefaultAssetBundle.of(context).loadString(assetNameCityData).then((data) {
      final List decoded = json.decode(data);
      final List _cities = decoded.map((cityJson) {
        final _state = cityJson['state'];
        final _name = (_state == null) ? cityJson['name'] : '${cityJson['name']}, $_state';
        return City(_name, cityJson['id'].toInt(), getCountry(cityJson['country']), cityJson['lat'], cityJson['lon']);
      }).toList(growable: false);
      cities = Cities(_cities);
    });
  }

  bool isAbleToSelectCity() {
    var timestampsOfCitySelections = prefs.getStringList(prefsKeyTimestampsOfCitySelections) ?? [];

    final _now = DateTime.now();
    timestampsOfCitySelections.insert(0, _now.millisecondsSinceEpoch.toString());
    timestampsOfCitySelections.retainWhere((timestamp) {
      final _timestamp = DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp));
      return _now.difference(_timestamp).inMinutes <= 60;
    });

    final oldest = DateTime.fromMillisecondsSinceEpoch(int.parse(timestampsOfCitySelections.last));
    final secondsSinceOldestSearch = _now.difference(oldest).inSeconds;
    final minSecondsSinceOldestSearch = math.pow(timestampsOfCitySelections.length - 1, 2);
    if (secondsSinceOldestSearch < minSecondsSinceOldestSearch) {
      return false;
    }

    var _isAbleToSelectCity = true;
    timestampsOfCitySelections.asMap().forEach((index, timestamp) {
      if (index >= 1) {
        final _timestamp = DateTime.fromMillisecondsSinceEpoch(int.parse(timestamp));
        final newerTimestamp = timestampsOfCitySelections[index - 1];
        final _newerTimestamp = DateTime.fromMillisecondsSinceEpoch(int.parse(newerTimestamp));
        final diffMillis = _newerTimestamp.difference(_timestamp).inMilliseconds;
        if (diffMillis < 100) {
          _isAbleToSelectCity = false;
        }
      }
    });
    if (_isAbleToSelectCity) {
      prefs.setStringList(prefsKeyTimestampsOfCitySelections, timestampsOfCitySelections);
    }
    return _isAbleToSelectCity;
  }

  showNotAllowedAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("You're picking cities rather quickly..."),
          content: Text("In order to ensure that bots don't abuse our system, we don't allow rapid city-selection.  "
              "This isn't an issue for normal users, but if you need us to relax this rule, please get in touch.\n\n"
              "never.ads.info@gmail.com"),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class MessageCanTapLocationName extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MessageCanTapLocationNameState();
}

class MessageCanTapLocationNameState extends State<MessageCanTapLocationName> {
  double opacity = 1.0;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: opacity,
      duration: Duration(milliseconds: 100),
      child: Card(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(16, 32, 16, 0),
              child: Text(
                'On this screen, you can add, remove, and reselect locations.\n\n'
                'Additionally, back on the main screen, you can tap the city-name at the top to more quickly change locations.\n\n'
                'Just thought you should know.  😘',
              ),
            ),
            CupertinoButton(
              child: Align(
                alignment: Alignment.bottomRight,
                child: Text(
                  'Got it!',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              onPressed: () async {
                prefs.setBool(prefsKeyShouldDisplayAlternateButtonMessage, false);
                setState(() => opacity = 0.0);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class UseCurrentLocationRow extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => UseCurrentLocationRowState();
}

class UseCurrentLocationRowState extends State<UseCurrentLocationRow> {
  bool isTapped = false;

  @override
  Widget build(BuildContext context) {
    return CupertinoButton(
      padding: EdgeInsets.fromLTRB(16, 16, 4, 16),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 24,
            height: 24,
            child: isTapped
                ? PlayAnimation(
                    builder: (BuildContext context, Widget child, value) => Opacity(
                      opacity: value,
                      child: CircularProgressIndicator(strokeWidth: 3),
                    ),
                    tween: Tween(begin: 0.0, end: 1.0),
                    duration: Duration(milliseconds: 1000),
                    delay: Duration(milliseconds: 200),
                    curve: Curves.easeInSine,
                  )
                : Icon(Icons.location_on, color: Theme.of(context).accentColor),
          ),
          SizedBox(width: 16),
          Flexible(
            child: Text(
              'Use my location',
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
        ],
      ),
      onPressed: () async {
        if (await isLocationEnabled()) {
          if (await hasLocationPermission()) {
            setState(() => isTapped = true);
            wasUseLocationSelected = true;
            prefs.setBool(prefsKeyWasUseLocationSelected, wasUseLocationSelected);
            final newId = await getLocationIdAsString();
            final newName = newId.split(sep).last;
            for (final oldId in searchedIds) {
              final oldName = oldId.split(sep).last;
              if (oldName == newName) {
                final previousData = prefs.getStringList(oldId);
                prefs.setStringList(newId, previousData);
                prefs.remove(oldId);
                searchedIds.remove(oldId);
                break;
              }
            }
            return selectLocation(newId, context);
          } else {
            ScaffoldMessenger.of(context).removeCurrentSnackBar();
            return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds: 6),
              content: Row(
                children: <Widget>[
                  Icon(Icons.error, size: 36, color: Theme.of(context).errorColor),
                  SizedBox(width: 12),
                  Flexible(
                    child: Text(messageGrantPermission),
                  ),
                ],
              ),
            ));
          }
        } else {
          ScaffoldMessenger.of(context).removeCurrentSnackBar();
          return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 6),
            content: Row(
              children: <Widget>[
                Icon(Icons.error, size: 36, color: Theme.of(context).errorColor),
                SizedBox(width: 12),
                Flexible(
                  child: Text(messageEnableLocation),
                ),
              ],
            ),
          ));
        }
      },
    );
  }
}

selectLocation(String locationId, BuildContext context) {
  alerts.clear();
  prefs.setBool(prefsKeyWasUseLocationSelected, wasUseLocationSelected);
  searchedIds.remove(locationId);
  searchedIds.insert(0, locationId);
  prefs.setStringList(prefsKeySearchedIds, searchedIds);
  Navigator.pop(context);
}

launchOpenSourceWebpage() async {
  const url = 'https://' + urlJustWeatherRepo;
  if (await canLaunch(url)) {
    await launch(url);
  }
}
