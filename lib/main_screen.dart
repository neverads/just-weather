import 'dart:convert';
import 'dart:io' show Platform;
import 'dart:math';

import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:myapp/info_screen.dart';
import 'package:myapp/weather_now_details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_animations/simple_animations.dart';

import 'alert.dart';
import 'config.dart';
import 'constants.dart';
import 'data_helpers.dart';
import 'globals.dart';
import 'location_selection_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MainScreenState();
  }
}

class MainScreenState extends State<MainScreen> with WidgetsBindingObserver {
  static const platform = const MethodChannel('just_weather.never_ads/location');

  bool isCelsius;
  bool is24HourClock;
  bool isFirstLaunch;

  bool isRefreshing = false;

  WeatherInfo weatherNow;
  HourlyForecast hourlyForecast;
  DailyForecast dailyForecast;
  String cityName;

  bool isRetryingConnection = false;
  bool isAppPaused = false;

  int tabIndex = 0;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    SharedPreferences.getInstance().then((SharedPreferences _prefs) {
      prefs = _prefs;
      final isLocaleUSA = Localizations.localeOf(context) == Locale('en', 'US');
      isCelsius = prefs.getBool(prefsKeyIsCelsius) ?? !isLocaleUSA;
      is24HourClock = prefs.getBool(prefsKeyIs24HourClock) ?? !isLocaleUSA;
      searchedIds = _prefs.getStringList(prefsKeySearchedIds) ?? [];
      isFirstLaunch = _prefs.getBool(prefsKeyIsFirstLaunch) ?? true;
      wasUseLocationSelected = _prefs.getBool(prefsKeyWasUseLocationSelected) ?? false;
      whenLocationUsed = _prefs.getInt(prefsKeyWhenLocationUsed);
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      isAppPaused = true;
    } else if (state == AppLifecycleState.resumed && isAppPaused) {
      isAppPaused = false;

      if (wasUseLocationSelected && whenLocationUsed == null) {
        whenLocationUsed = DateTime.now().millisecondsSinceEpoch;
        prefs.setInt(prefsKeyWhenLocationUsed, whenLocationUsed);
        updateDataForNewLocation().then((_) => setState(() {}));
      } else {
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final popupMenuValueChangeLocation = 0;
    final popupMenuValueAppInfo = 1;

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: FutureBuilder(
          future: awaitAppBarTitleReady(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (searchedIds.length <= 1) {
                return cityNameNavButton;
              } else {
                try {
                  return DropdownButton(
                    value: searchedIds.first,
                    dropdownColor: Theme.of(context).primaryColor,
                    items: searchedIds.map((idAsString) {
                      final isFirst = searchedIds.indexOf(idAsString) == 0;
                      return DropdownMenuItem(
                        value: idAsString,
                        child: Text(
                          getShortCityName(City.fromIdAsString(idAsString).name),
                          style: isFirst
                              ? textStyleAppBarTitle
                              : textStyleAppBarTitle.copyWith(fontWeight: FontWeight.normal),
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                    onChanged: (newValue) => setState(() {
                      alerts.clear();
                      prefs.setBool(prefsKeyWasUseLocationSelected, wasUseLocationSelected);
                      searchedIds.remove(newValue);
                      searchedIds.insert(0, newValue);
                      prefs.setStringList(prefsKeySearchedIds, searchedIds);
                    }),
                    underline: Container(),
                    icon: Container(),
                    isExpanded: true,
                  );
                } catch (e) {
                  return cityNameNavButton;
                }
              }
            } else {
              return Container();
            }
          },
        ),
        actions: <Widget>[
          FutureBuilder(
            future: awaitAppBarButtonsReady(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Builder(
                  builder: (context) => cityName == appTitle
                      ? SizedBox()
                      : IconButton(
                          icon: Icon(Icons.refresh, color: textColorAppBarPrimary),
                          onPressed: () async {
                            if (wasUseLocationSelected && whenLocationUsed == null) {
                              whenLocationUsed = DateTime.now().millisecondsSinceEpoch;
                              prefs.setInt(prefsKeyWhenLocationUsed, whenLocationUsed);
                              await updateDataForNewLocation();
                            }
                            setState(() => isRefreshing = true);
                          },
                        ),
                );
              } else {
                return Text("");
              }
            },
          ),
          celsiusFahrenheitToggleWidget,
          FutureBuilder(
            future: awaitAppBarButtonsReady(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Builder(
                  builder: (context) => cityName == appTitle
                      ? SizedBox()
                      : IconButton(
                          icon: Icon(
                            is24HourClock ? Icons.access_time : Icons.watch_later,
                            color: textColorAppBarPrimary,
                          ),
                          onPressed: () => setState(() {
                            is24HourClock = !is24HourClock;
                            final messageBody = is24HourClock ? "24-hour" : "AM / PM";
                            prefs.setBool(prefsKeyIs24HourClock, is24HourClock);
                            _scaffoldKey.currentState.removeCurrentSnackBar();
                            _scaffoldKey.currentState.showSnackBar(
                              SnackBar(
                                duration: Duration(seconds: 3),
                                content: Text('$messageBody time display'),
                              ),
                            );
                          }),
                        ),
                );
              } else {
                return Text("");
              }
            },
          ),
          PopupMenuButton(
            itemBuilder: (context) => [
              cityName == appTitle
                  ? null
                  : PopupMenuItem(
                      value: popupMenuValueChangeLocation,
                      child: Text('Change Location'),
                    ),
              PopupMenuItem(
                value: popupMenuValueAppInfo,
                child: Text(infoScreenTitle),
              ),
            ],
            onSelected: (value) async {
              if (value == popupMenuValueChangeLocation) {
                final shouldDisplayMessage = prefs.getBool(prefsKeyShouldDisplayAlternateButtonMessage) ?? true;
                await Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => LocationSelectionScreen(shouldDisplayMessage),
                    maintainState: false,
                  ),
                );
                setState(() {});
              } else if (value == popupMenuValueAppInfo) {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => InfoScreen()));
              }
            },
          ),
        ],
      ),
      body: FutureBuilder(
        future: fetchWeatherInfo(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              final errorText = snapshot.error.toString().replaceAll('Exception: ', '').split('\n');
              return Padding(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Visibility(
                      visible: isFirstLaunch,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(16),
                            child: Column(
                              children: <Widget>[
                                Text('Which would you prefer?', style: Theme.of(context).textTheme.headline5),
                                SizedBox(height: 4),
                                Text('($messageNoDataCollection)', style: Theme.of(context).textTheme.bodyText2),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: !isFirstLaunch,
                      child: Padding(
                        padding: EdgeInsets.all(16),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(Icons.error_outline, size: 32, color: Theme.of(context).errorColor),
                            SizedBox(width: 16),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(errorText.first, style: Theme.of(context).textTheme.headline6),
                                  Visibility(
                                    visible: errorText.length > 1,
                                    child: Column(
                                      children: [
                                        SizedBox(height: 16),
                                        Text(
                                          'Details:\n\n' + errorText.sublist(1).join('\n\n'),
                                          style: Theme.of(context).textTheme.caption,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: CupertinoButton(
                            child: Column(
                              children: <Widget>[
                                Icon(
                                  isFirstLaunch ? Icons.location_on : Icons.refresh,
                                  size: 32,
                                  color: Theme.of(context).accentColor,
                                ),
                                SizedBox(height: 8),
                                Text(
                                  isFirstLaunch ? 'use location' : 'retry location',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                            onPressed: () async {
                              wasUseLocationSelected = true;
                              prefs.setBool(prefsKeyWasUseLocationSelected, wasUseLocationSelected);
                              if (isFirstLaunch) {
                                isFirstLaunch = false;
                                prefs.setBool(prefsKeyIsFirstLaunch, false);
                              } else {
                                await updateDataForNewLocation();
                              }
                              setState(() {});
                            },
                          ),
                        ),
                        Expanded(
                          child: CupertinoButton(
                            child: Column(
                              children: <Widget>[
                                Icon(Icons.location_city, size: 32, color: Theme.of(context).accentColor),
                                SizedBox(height: 8),
                                Text(
                                  'select a city',
                                  style: Theme.of(context).textTheme.headline4,
                                ),
                              ],
                            ),
                            onPressed: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(
                                      builder: (context) => LocationSelectionScreen(false), maintainState: false))
                                  .then((_) {
                                if (isFirstLaunch) {
                                  isFirstLaunch = false;
                                  prefs.setBool(prefsKeyIsFirstLaunch, false);
                                }
                                setState(() {});
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            } else {
              return loadLayoutConnectionSuccess();
            }
          } else {
            return Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 48),
                      child: PlayAnimation(
                        builder: (BuildContext context, Widget child, value) => Opacity(
                          opacity: value,
                          child: CircularProgressIndicator(strokeWidth: 3),
                        ),
                        tween: Tween(begin: 0.0, end: 1.0),
                        duration: Duration(milliseconds: 400),
                      ),
                    )
                  ],
                ),
                PlayAnimation(
                  builder: (BuildContext context, Widget child, value) {
                    return Opacity(
                      opacity: value,
                      child: Text('fetching weather data…', style: Theme.of(context).textTheme.headline6),
                    );
                  },
                  tween: Tween(begin: 0.0, end: 1.0),
                  duration: Duration(seconds: 2),
                  delay: Duration(milliseconds: 500),
                  curve: Curves.easeInToLinear,
                ),
              ],
            );
          }
        },
      ),
    );
  }

  Future awaitAppBarButtonsReady() async {
    const int maxMillis = 50;
    var i = 1;
    while (isCelsius == null || is24HourClock == null) {
      await Future.delayed(Duration(milliseconds: i));
      if (i < maxMillis) {
        i += i;
      } else {
        i = maxMillis;
      }
    }
  }

  Future awaitAppBarTitleReady() async {
    if (!isRefreshing) {
      const int maxMillis = 50;
      cityName = "";
      var i = 1;
      while (cityName == "") {
        await Future.delayed(Duration(milliseconds: i));
        if (i < maxMillis) {
          i += i;
        } else {
          i = maxMillis;
        }
      }
    }
  }

  Widget get celsiusFahrenheitToggleWidget {
    return FutureBuilder(
      future: awaitAppBarButtonsReady(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return cityName == appTitle
              ? SizedBox()
              : CupertinoButton(
                  child: Row(
                    children: <Widget>[
                      Text(isCelsius ? '˚C' : '˚F', style: textStyleAppBarPrimary),
                      Text('/${isCelsius ? 'F' : 'C'}', style: textStyleAppBarSecondary),
                    ],
                    textBaseline: TextBaseline.alphabetic,
                    crossAxisAlignment: CrossAxisAlignment.baseline,
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 8),
                  onPressed: () {
                    setState(() {
                      isCelsius = !isCelsius;
                      prefs.setBool(prefsKeyIsCelsius, isCelsius);
                      _scaffoldKey.currentState.removeCurrentSnackBar();
                      _scaffoldKey.currentState.showSnackBar(
                        SnackBar(
                          content: Text(
                            isCelsius ? 'Forecast displayed in Celsius' : 'Forecast displayed in Fahrenheit',
                          ),
                        ),
                      );
                    });
                    if (Platform.isAndroid) {
                      _sendIsCelsiusAndLocationIdForAndroidWidget();
                    }
                  },
                );
        } else {
          return Text("");
        }
      },
    );
  }

  Row getHourlyForecastUI() {
    if (weatherNow == null) {
      return null;
    }

    Row hourlyForecastUI = Row(children: <Widget>[]);

    final forecastWidgetsCount = 48; // enough for 48 hours
    final shouldSkipFirst = hourlyForecast.dataPoints.first.hour == hourlyForecast.dataPoints[1].hour;
    var startingIndex = shouldSkipFirst ? 1 : 0;
    final now = DateTime.now();
    if (hourlyForecast.dataPoints.first.hour <= now.hour && hourlyForecast.dataPoints.first.day == now.day) {
      startingIndex++;
    }
    final maxTemp = hourlyForecast.maxTemp;
    final minTemp = hourlyForecast.minTemp;
    for (var i = startingIndex; i < forecastWidgetsCount; i++) {
      final dataPoint = hourlyForecast.dataPoints[i];
      final precedingTemp = i == startingIndex ? null : hourlyForecast.dataPoints[i - 1].temperatureCelsius;
      final comingTemp = i == forecastWidgetsCount - 1 ? null : hourlyForecast.dataPoints[i + 1].temperatureCelsius;
      hourlyForecastUI.children.add(_getHourlyForecastWidget(dataPoint, precedingTemp, comingTemp, maxTemp, minTemp));
    }

    return hourlyForecastUI;
  }

  CupertinoButton _getHourlyForecastWidget(
      HourlyForecastDataPoint dataPoint, int precedingTemp, int comingTemp, int maxTemp, int minTemp) {
    int temperature = isCelsius ? dataPoint.temperatureCelsius : dataPoint.temperatureFahrenheit;
    String hourSuffix;
    int _hour = dataPoint.hour;

    if (is24HourClock) {
      hourSuffix = ':00';
    } else {
      hourSuffix = dataPoint.hour < 12 ? ' AM' : ' PM';
      if (dataPoint.hour == 0) {
        _hour = 12;
      } else if (dataPoint.hour > 12) {
        _hour = dataPoint.hour - 12;
      }
    }

    const lineThickness = 1.0;
    const extraMargin = 8.0;
    final tempDiff = maxTemp - minTemp;
    final baseMargin = 24;
    final precedingTempChange =
        dataPoint.temperatureCelsius - (precedingTemp == null ? weatherNow.temperatureCurrentCelsius : precedingTemp);
    final comingTempChange = comingTemp == null ? 0.0 : comingTemp - dataPoint.temperatureCelsius;
    final angleMultiplier = (tempDiff * tempDiff) / (tempDiff / -3);
    final precedingAngle = precedingTemp == null ? 0.0 : precedingTempChange / angleMultiplier;
    final comingAngle = comingTemp == null ? 0.0 : comingTempChange / angleMultiplier;
    final percentageOfTempDiff = (dataPoint.temperatureCelsius - minTemp) / tempDiff;
    final precedingLineAllowance = (precedingTempChange == 0
        ? 0
        : precedingTempChange > 0
            ? lineThickness / 2
            : lineThickness / -2);
    final comingLineAllowance = (comingTempChange == 0
        ? 0
        : comingTempChange > 0
            ? lineThickness / 2
            : lineThickness / -2);
    final maxTempBoost = temperature == maxTemp ? lineThickness * 2 : 0;
    final precedingBottomMargin =
        ((baseMargin - precedingTempChange) * percentageOfTempDiff) - precedingLineAllowance + maxTempBoost;
    final comingBottomMargin =
        ((baseMargin + comingTempChange) * percentageOfTempDiff) + comingLineAllowance + maxTempBoost;
    final topMargin = baseMargin - [precedingBottomMargin, comingBottomMargin].reduce(max);
    final bottomMarginOverride = precedingBottomMargin == 0 && comingBottomMargin == 0 ? 1 : null;

    final horizontalMargin = 6.0;
    final sectionWidth = 16.0;

    return CupertinoButton(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: [topMargin, 0.0].reduce(max)),
            child: Text(' $temperature˚', style: Theme.of(context).textTheme.headline6),
          ),
          Row(
            children: [
              Transform.rotate(
                angle: precedingAngle,
                child: Container(
                  margin: EdgeInsets.only(
                    left: horizontalMargin,
                    top: extraMargin,
                    right: horizontalMargin,
                    bottom: (bottomMarginOverride ?? precedingBottomMargin) + extraMargin,
                  ),
                  decoration: BoxDecoration(color: Theme.of(context).primaryColorDark),
                  child: SizedBox(width: sectionWidth, height: lineThickness),
                ),
              ),
              Transform.rotate(
                angle: comingAngle,
                child: Container(
                  margin: EdgeInsets.only(
                    left: horizontalMargin,
                    top: extraMargin,
                    right: horizontalMargin,
                    bottom: (bottomMarginOverride ?? comingBottomMargin) + extraMargin,
                  ),
                  decoration: BoxDecoration(color: Theme.of(context).primaryColorDark),
                  child: SizedBox(width: sectionWidth, height: lineThickness),
                ),
              ),
            ],
          ),
          SizedBox(height: 12),
          Opacity(
            opacity: [0.5 + (0.7 * dataPoint.chanceOfPrecip / 100), 1.0].reduce(min),
            child: Row(
              children: [
                Text(
                  ' ${dataPoint.chanceOfPrecip}',
                  style:
                      Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.blue, fontWeight: FontWeight.bold),
                ),
                Text(
                  '%',
                  style: Theme.of(context).textTheme.caption.copyWith(color: Colors.blue, fontWeight: FontWeight.bold),
                ),
              ],
              textBaseline: TextBaseline.alphabetic,
              crossAxisAlignment: CrossAxisAlignment.baseline,
            ),
          ),
          SizedBox(
            width: weatherIconHourlyBoxSize,
            height: weatherIconHourlyBoxSize,
            child: Padding(
              padding: EdgeInsets.all(weatherIconPadding),
              child:
                  getWeatherIcon(dataPoint.weatherIconCode, dataPoint.cloudinessPercentage, dataPoint.chanceOfPrecip),
            ),
          ),
          SizedBox(height: 8),
          Row(
            children: [
              Text('$_hour', style: Theme.of(context).textTheme.subtitle1),
              Text(hourSuffix, style: Theme.of(context).textTheme.caption),
            ],
            textBaseline: TextBaseline.alphabetic,
            crossAxisAlignment: CrossAxisAlignment.baseline,
          ),
        ],
      ),
      onPressed: () {
        _scaffoldKey.currentState.removeCurrentSnackBar();
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              '$_hour$hourSuffix\n\n'
              '${dataPoint.weatherDescription}\n'
              '${dataPoint.chanceOfPrecip}% chance of precipitation',
            ),
          ),
        );
      },
    );
  }

  Widget getDailyForecastUI() {
    if (weatherNow == null) {
      return null;
    }

    return ListView.separated(
      itemCount: dailyForecast.dataPoints.length,
      primary: false,
      physics: AlwaysScrollableScrollPhysics(),
      padding: EdgeInsets.fromLTRB(24, 8, 24, 88),
      itemBuilder: (context, index) {
        final dataPoint = dailyForecast.dataPoints[index];
        final date = DateTime.now().add(Duration(days: index));
        final title = index == 0
            ? 'Today'
            : '${DateFormat('${DateFormat.ABBR_WEEKDAY}, ${DateFormat.DAY} ${DateFormat.ABBR_MONTH}').format(date)}';
        final high = isCelsius ? dataPoint.highCelsius : dataPoint.highFahrenheit;
        final low = isCelsius ? dataPoint.lowCelsius : dataPoint.lowFahrenheit;
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(title, style: Theme.of(context).textTheme.headline6),
                  SizedBox(height: 8),
                  Text(dataPoint.weatherDescription, style: Theme.of(context).textTheme.subtitle1),
                ],
              ),
              Spacer(),
              Opacity(
                opacity: [0.5 + (0.7 * dataPoint.chanceOfPrecip / 100), 1.0].reduce(min),
                child: CupertinoButton(
                  child: Text(
                    '${dataPoint.chanceOfPrecip}%',
                    style:
                        Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    _scaffoldKey.currentState.removeCurrentSnackBar();
                    _scaffoldKey.currentState.showSnackBar(
                      SnackBar(
                        content: Text('Chance of precipitation: ${dataPoint.chanceOfPrecip}%'),
                      ),
                    );
                  },
                ),
              ),
              getWeatherIcon(dataPoint.weatherIconCode, dataPoint.cloudinessPercentage, dataPoint.chanceOfPrecip,
                  weatherIconDailyBoxSize),
              SizedBox(width: 24),
              Column(
                children: [
                  Text('$high˚', style: Theme.of(context).textTheme.headline6),
                  SizedBox(height: 8),
                  Text('$low˚', style: Theme.of(context).textTheme.subtitle1),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (context, index) => Divider(),
    );
  }

  loadLayoutConnectionSuccess() {
    int fetchesCountdown = prefs.getInt(prefsKeyFetchesCount) ?? 10;
    if (fetchesCountdown > 0) {
      prefs.setInt(prefsKeyFetchesCount, --fetchesCountdown);
    }

    const tabBarHeight = 48.0;
    final padding = MediaQuery.of(context).padding;
    final screenHeight =
        MediaQuery.of(context).size.height - padding.top - padding.bottom - AppBar().preferredSize.height;
    final _controller = ScrollController();
    final standardFadingEdge = 0.1;
    var _tabs = [
      Tab(text: 'today'),
      Tab(text: 'this week'),
    ];
    var _tabsChildren = [
      ListView(
        padding: EdgeInsets.only(top: 8, bottom: 64),
        primary: false,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: mainScreenHorizontalPadding),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    todaysHighAndLowTempsWidget,
                    currentTempWidget,
                    feelsLikeWidget,
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 24),
                  child: nowWeatherIconWidget,
                ),
                SizedBox(),
                SizedBox(),
                SizedBox(),
              ],
            ),
          ),
          WeatherNowDetails(weatherNow, isCelsius, is24HourClock),
          Divider(height: 0, indent: mainScreenHorizontalPadding, endIndent: mainScreenHorizontalPadding),
          Padding(
            padding: EdgeInsets.fromLTRB(mainScreenHorizontalPadding, 16, mainScreenHorizontalPadding, 32),
            child: FadingEdgeScrollView.fromSingleChildScrollView(
              gradientFractionOnStart: standardFadingEdge,
              gradientFractionOnEnd: standardFadingEdge + (fetchesCountdown * 0.02),
              child: SingleChildScrollView(
                controller: _controller,
                scrollDirection: Axis.horizontal,
                child: getHourlyForecastUI(),
              ),
            ),
          ),
        ],
      ),
      getDailyForecastUI(),
    ];
    if (alerts.isNotEmpty) {
      _tabs.add(
        Tab(
          icon: Icon(
            Icons.warning,
            color: Theme.of(context).errorColor,
          ),
        ),
      );
      _tabsChildren.add(
        ListView.separated(
          itemCount: alerts.length,
          padding: EdgeInsets.fromLTRB(32, 24, 32, 64),
          itemBuilder: (context, index) => Column(
            children: [
              ListTile(
                title: Text(
                  alerts[index].title,
                  style: Theme.of(context).textTheme.headline4, //.copyWith(color: Theme.of(context).errorColor),
                ),
                subtitle: Text(
                  '\n${alerts[index].description}',
                  style: Theme.of(context).textTheme.subtitle1,
                ),
              ),
            ],
          ),
          separatorBuilder: (context, index) => Divider(height: 64),
        ),
      );
    }

    return DefaultTabController(
      initialIndex: tabIndex,
      length: _tabs.length,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: tabBarHeight,
            child: TabBar(
              onTap: (index) {
                tabIndex = index;
              },
              indicatorColor: Theme.of(context).accentColor,
              labelColor: Theme.of(context).accentColor,
              tabs: _tabs,
            ),
          ),
          SizedBox(
            height: screenHeight - tabBarHeight,
            child: TabBarView(
              children: _tabsChildren,
            ),
          ),
        ],
      ),
    );
  }

  Widget get todaysHighAndLowTempsWidget {
    final int highTemp = hourlyForecast.todaysHighTemp(isCelsius);
    final int lowTemp = hourlyForecast.todaysLowTemp(isCelsius);
    return CupertinoButton(
      child: Text(
        '↑ $highTemp˚     ↓ $lowTemp°',
        style: Theme.of(context).textTheme.headline6,
      ),
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(left: 8, bottom: 2),
      onPressed: () {
        _scaffoldKey.currentState.removeCurrentSnackBar();
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'High temperature: $highTemp˚\n\n'
              'Low temperature: $lowTemp˚\n\n\n'
              '(within the next $hoursToConsiderForHighAndLowTemps hours)',
            ),
          ),
        );
      },
    );
  }

  Widget get currentTempWidget {
    final TextStyle baseTextStyle =
        temperatureCurrent >= 100 ? Theme.of(context).textTheme.headline2 : Theme.of(context).textTheme.headline1;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: temperatureCurrent >= 100 ? 16 : 4),
      child: CupertinoButton(
        child: Text(
          '$temperatureCurrent˚',
          style: baseTextStyle.copyWith(
            color: Theme.of(context).primaryColorDark,
          ),
        ),
        padding: EdgeInsets.zero,
        onPressed: () {
          final now = DateTime.now();
          final recentDataTimestamp = prefs.getStringList(searchedIds.first).first;
          final whenFetched = DateTime.fromMillisecondsSinceEpoch(int.parse(recentDataTimestamp));
          final sinceFetched = now.difference(whenFetched);
          final since = (sinceFetched.inSeconds < 60)
              ? '${sinceFetched.inSeconds} second${sinceFetched.inSeconds == 1 ? '' : 's'}'
              : '${sinceFetched.inMinutes} minute${sinceFetched.inMinutes == 1 ? '' : 's'}';
          _scaffoldKey.currentState.removeCurrentSnackBar();
          _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text(
                'Current temperature:  $temperatureCurrent˚\n\n'
                'Fresh data was fetched $since ago at ${getTime(whenFetched, is24HourClock)}.',
              ),
            ),
          );
        },
      ),
    );
  }

  Widget get feelsLikeWidget {
    final int temperatureFeelsLike = weatherNow == null
        ? '--'
        : isCelsius
            ? weatherNow.temperatureFeelsLikeCelsius
            : weatherNow.temperatureFeelsLikeFahrenheit;
    return Padding(
      padding: EdgeInsets.only(left: 8),
      child: Text(
        'Feels like:  $temperatureFeelsLike˚',
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget get nowWeatherIconWidget {
    final double size = temperatureCurrent >= 100 ? 72.0 : 96.0;
    return CupertinoButton(
      child: SizedBox(
        width: size,
        height: size,
        child: getNowWeatherIcon(weatherNow.weatherIconCode, weatherNow.cloudinessPercentage),
      ),
      onPressed: () {
        final suffix = weatherNow.weatherId >= 200 && weatherNow.weatherId < 800 ? ' (high chance)' : '';
        _scaffoldKey.currentState.removeCurrentSnackBar();
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text('Now\n\n${weatherNow.weatherDescription}$suffix')),
        );
      },
    );
  }

  int get temperatureCurrent => weatherNow == null
      ? '--'
      : isCelsius
          ? weatherNow.temperatureCurrentCelsius
          : weatherNow.temperatureCurrentFahrenheit;

  setCityNameFromFirstSearchedId() {
    cityName = getShortCityName(searchedIds.first.split(sep).last);
  }

  fetchWeatherInfo() async {
    var i = 1;
    while (searchedIds == null || isFirstLaunch == null) {
      await Future.delayed(Duration(microseconds: i));
      i += i;
    }

    if (isFirstLaunch) {
      weatherNow = null;
      hourlyForecast = null;
      dailyForecast = null;
      alerts.clear();
      cityName = appTitle;
      throw Exception(prefsKeyIsFirstLaunch);
    }

    await clearOutdatedData();

    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    List<String> previousWeatherInfo;
    if (searchedIds.isNotEmpty) {
      previousWeatherInfo = prefs.getStringList(searchedIds.first);
    }
    if (previousWeatherInfo == null) {
      List<WeatherInfo> weatherInfo;

      var response;
      try {
        double lat;
        double long;
        if (searchedIds.isEmpty) {
          if (location == null) {
            location = Location();
            location.changeSettings(accuracy: locationAccuracy);
          }
          if (await isLocationEnabled()) {
            if (await hasLocationPermission()) {
              await location.changeSettings(accuracy: locationAccuracy);
              final locationData = await location.getLocation();
              lat = locationData.latitude;
              long = locationData.longitude;

              final newId = await getLocationIdAsString();
              searchedIds.insert(0, newId);
              prefs.setStringList(prefsKeySearchedIds, searchedIds);
            } else {
              _scaffoldKey.currentState.removeCurrentSnackBar();
              throw Exception(messageGrantPermission);
            }
          } else {
            _scaffoldKey.currentState.removeCurrentSnackBar();
            throw Exception(messageEnableLocation);
          }
        } else {
          final split = searchedIds.first.split(sep);
          lat = double.parse(split.first);
          long = double.parse(split[1]);
        }
        response = await http.get(
            'https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&exclude=minutely&appid=$OWM_API_KEY');
      } catch (error) {
        weatherNow = null;
        hourlyForecast = null;
        dailyForecast = null;
        alerts.clear();
        cityName = appTitle;
        if (error.toString().contains(messageEnableLocation)) {
          throw Exception(error);
        } else {
          throw Exception('Unable to fetch weather data.  Please check your network connection.\n'
              '$error');
        }
      }

      if (response.statusCode == 200) {
        hasConnection = true;
        final body = json.decode(response.body);
        final timezoneOffsetFromApi = body['timezone_offset'];
        final now = WeatherInfo.fromJson(body['current'], timezoneOffsetFromApi);
        weatherInfo = [now];

        List<WeatherInfo> _hourlyDataPoints = [];
        body['hourly']
            .forEach((dataPoint) => _hourlyDataPoints.add(WeatherInfo.fromJson(dataPoint, timezoneOffsetFromApi)));

        List<WeatherInfo> _dailyDataPoints = [];
        body['daily']
            .forEach((dataPoint) => _dailyDataPoints.add(WeatherInfo.fromJson(dataPoint, timezoneOffsetFromApi)));

        weatherNow = weatherInfo.first;
        hourlyForecast = HourlyForecast(_hourlyDataPoints);
        dailyForecast = DailyForecast(_dailyDataPoints);
        alerts.clear();
        final List _alerts = body['alerts'];
        if (_alerts != null && _alerts.isNotEmpty) {
          _alerts.forEach((_alert) => alerts.add(Alert.fromJson(_alert)));
        }
        setCityNameFromFirstSearchedId();

        final timestamp = DateTime.now().millisecondsSinceEpoch.toString();
        final previousWeatherData = [timestamp, weatherNow.toStringForPreferences()] +
            hourlyForecast.asStringsForPreferences +
            dailyForecast.asStringsForPreferences;
        prefs.setStringList(searchedIds.first, previousWeatherData);
      } else {
        weatherNow = null;
        hourlyForecast = null;
        dailyForecast = null;
        cityName = null;
        throw Exception('Error fetching weather data.\n'
            'Please take a screenshot and email it to never.ads.info@gmail.com.\n'
            "We'll fix it ASAP.\n\n"
            'Response code: "${response.statusCode}"');
      }
    } else {
      weatherNow = WeatherInfo.fromString(previousWeatherInfo[1]);
      hourlyForecast = HourlyForecast.fromStrings(previousWeatherInfo.sublist(2, 50));
      dailyForecast = DailyForecast.fromStrings(previousWeatherInfo.sublist(50));
      setCityNameFromFirstSearchedId();
      if (isRefreshing) {
        isRefreshing = false;
        await Future.delayed(Duration(milliseconds: 500));
      }
    }

    if (Platform.isAndroid) {
      await _updateWidgetWithoutAnotherFetch();
      _sendIsCelsiusAndLocationIdForAndroidWidget();
    }
  }

  Future<void> clearOutdatedData() async {
    final minutesThreshold = 10;

    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
    final now = DateTime.now();

    if (whenLocationUsed != null) {
      DateTime _whenLocationUsed = DateTime.fromMillisecondsSinceEpoch(whenLocationUsed);
      if (now.difference(_whenLocationUsed).inMinutes > minutesThreshold) {
        whenLocationUsed = null;
        prefs.remove(prefsKeyWhenLocationUsed);
      }
    }

    for (final id in searchedIds) {
      final previousWeatherInfo = prefs.getStringList(id);
      if (previousWeatherInfo != null) {
        int timestamp = int.parse(previousWeatherInfo.first);
        DateTime whenDataWasFetched = DateTime.fromMillisecondsSinceEpoch(timestamp);
        bool isUpToDate = now.difference(whenDataWasFetched).inMinutes <= minutesThreshold;
        if (!isUpToDate) {
          prefs.remove(id);
        }
      }
    }
  }

  Future<void> _sendIsCelsiusAndLocationIdForAndroidWidget() async {
    try {
      await platform.invokeMethod(
        'setIsCelsiusAndLocationId',
        <String, dynamic>{
          'isCelsius': isCelsius ?? true,
          'locationId': searchedIds.first ?? '',
        },
      );
    } on PlatformException catch (e) {
      print("PLATFORM EXCEPTION -- $e");
    }
  }

  Future<void> _updateWidgetWithoutAnotherFetch() async {
    var _highTemp;
    var _lowTemp;
    final isFirstDailyToday = dailyForecast.dataPoints.first.weekday == getWeekdayAsString(DateTime.now().weekday);
    if (isFirstDailyToday) {
      _highTemp =
          isCelsius ? dailyForecast.dataPoints.first.highCelsius : dailyForecast.dataPoints.first.highFahrenheit;
      _lowTemp = isCelsius ? dailyForecast.dataPoints.first.lowCelsius : dailyForecast.dataPoints.first.lowFahrenheit;
    } else {
      List<int> hourlyTemps = [];
      final dataPoints = hourlyForecast.dataPoints.sublist(0, 18); // don't override today's high/low with tomorrow's
      for (final dataPoint in dataPoints) {
        hourlyTemps.add(isCelsius ? dataPoint.temperatureCelsius : dataPoint.temperatureFahrenheit);
      }
      _highTemp = hourlyTemps.reduce(max);
      _lowTemp = hourlyTemps.reduce(min);
    }

    try {
      await platform.invokeMethod(
        'updateWidgetWithoutAnotherFetch',
        <String, dynamic>{
          'currentTemp': isCelsius ? weatherNow.temperatureCurrentCelsius : weatherNow.temperatureCurrentFahrenheit,
          'highTemp': _highTemp,
          'lowTemp': _lowTemp,
          'weatherIconCode': weatherNow.weatherIconCode,
          'cloudinessPercentage': weatherNow.cloudinessPercentage,
        },
      );
    } on PlatformException catch (e) {
      print("PLATFORM EXCEPTION -- $e");
    }
  }

  TextButton get cityNameNavButton => TextButton(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Text(
            getSafeTitle(cityName),
            style: textStyleAppBarTitle,
            overflow: TextOverflow.ellipsis,
            maxLines: 2,
          ),
        ),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => LocationSelectionScreen(false), maintainState: false))
              .then((_) => setState(() {}));
        },
      );
}

updateDataForNewLocation() async {
  if (await isLocationEnabled() && await hasLocationPermission()) {
    final newId = await getLocationIdAsString();
    final newName = newId.split(sep).last;
    for (final oldId in searchedIds) {
      final oldName = oldId.split(sep).last;
      if (oldName == newName) {
        final previousData = prefs.getStringList(oldId);
        prefs.setStringList(newId, previousData);
        prefs.remove(oldId);
        searchedIds.remove(oldId);
        break;
      }
    }
    searchedIds.remove(newId);
    searchedIds.insert(0, newId);
    prefs.setStringList(prefsKeySearchedIds, searchedIds);
  }
}
