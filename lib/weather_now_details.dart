import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:myapp/globals.dart';

import 'constants.dart';
import 'data_helpers.dart';

class WeatherNowDetails extends StatefulWidget {
  final weatherNow;
  final isCelsius;
  final is24HourClock;

  WeatherNowDetails(this.weatherNow, this.isCelsius, this.is24HourClock);

  @override
  State<StatefulWidget> createState() {
    return _WeatherNowDetailsState();
  }
}

class _WeatherNowDetailsState extends State<WeatherNowDetails> {
  bool shouldDisplayNowDetails = prefs.getBool(prefsKeyShouldDisplayNowDetails) ?? true;
  bool shouldDisplayPreferences = false;
  bool shouldDisplayNowDetailHumidity = prefs.getBool(prefsKeyShouldDisplayNowDetailHumidity) ?? true;
  bool shouldDisplayNowDetailWind = prefs.getBool(prefsKeyShouldDisplayNowDetailWind) ?? true;
  bool shouldDisplayNowDetailSunrise = prefs.getBool(prefsKeyShouldDisplayNowDetailSunrise) ?? true;
  bool shouldDisplayNowDetailSunset = prefs.getBool(prefsKeyShouldDisplayNowDetailSunset) ?? true;

  final GlobalKey<AnimatedListState> _listKey = GlobalKey();
  final List listItems = [];

  @override
  Widget build(BuildContext context) {
    const multiplierToMetersPerSecond = 0.28;
    const multiplierKilometerToMile = 0.62;
    final _windSpeed = widget.isCelsius || widget.weatherNow.windSpeedKPH == null
        ? (num.parse(widget.weatherNow.windSpeedKPH) * multiplierToMetersPerSecond).round()
        : (num.parse(widget.weatherNow.windSpeedKPH) * multiplierKilometerToMile).round();
    final _windGust = widget.weatherNow.windGustKPH == null
        ? null
        : widget.isCelsius
            ? (num.parse(widget.weatherNow.windGustKPH) * multiplierToMetersPerSecond).round()
            : (num.parse(widget.weatherNow.windGustKPH) * multiplierKilometerToMile).round();
    final windGustAddition = _windGust == null ? '' : '-$_windGust';

    final iconOpacityNowDetails = 0.9;
    final textStyleNowDetails = Theme.of(context).textTheme.subtitle1;
    final textStyleNowDetailsMinor = Theme.of(context).textTheme.bodyText2;
    final pictureSizeNowDetails = 18.0;
    final checkBoxSize = 20.0;

    List<Widget> nowDetailsWidgets = [];
    if (shouldDisplayPreferences || shouldDisplayNowDetailHumidity) {
      nowDetailsWidgets.add(
        CupertinoButton(
          padding: EdgeInsets.zero,
          child: Column(
            children: <Widget>[
              Opacity(
                opacity: iconOpacityNowDetails,
                child: SvgPicture.asset(
                  assetNameIconHumidity,
                  color: Theme.of(context).primaryColorDark,
                  height: pictureSizeNowDetails,
                  width: pictureSizeNowDetails,
                ),
              ),
              SizedBox(height: 8),
              Visibility(
                visible: !shouldDisplayPreferences,
                child: Row(
                  textBaseline: TextBaseline.alphabetic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: <Widget>[
                    Text('${widget.weatherNow.humidityPercentage}', style: textStyleNowDetails),
                    Text('%', style: textStyleNowDetailsMinor),
                  ],
                ),
              ),
              Visibility(
                visible: shouldDisplayPreferences,
                child: Icon(
                  shouldDisplayNowDetailHumidity ? Icons.check_box : Icons.check_box_outline_blank,
                  size: checkBoxSize,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
          onPressed: () {
            if (shouldDisplayPreferences) {
              setState(() {
                shouldDisplayNowDetailHumidity = !shouldDisplayNowDetailHumidity;
                prefs.setBool(prefsKeyShouldDisplayNowDetailHumidity, shouldDisplayNowDetailHumidity);
              });
            } else {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Humidity: ${widget.weatherNow.humidityPercentage}%')),
              );
            }
          },
        ),
      );
    }
    if (shouldDisplayPreferences || shouldDisplayNowDetailWind) {
      nowDetailsWidgets.add(
        CupertinoButton(
          padding: EdgeInsets.zero,
          child: Column(
            children: [
              Opacity(
                opacity: iconOpacityNowDetails,
                child: SvgPicture.asset(
                  assetNameIconWind,
                  color: Theme.of(context).primaryColorDark,
                  height: pictureSizeNowDetails,
                  width: pictureSizeNowDetails,
                ),
              ),
              SizedBox(height: 8),
              Visibility(
                visible: !shouldDisplayPreferences,
                child: Row(
                  textBaseline: TextBaseline.alphabetic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: <Widget>[
                    Text('$_windSpeed$windGustAddition', style: textStyleNowDetails),
                    Text(' ${widget.isCelsius ? 'm/s' : 'mph'}', style: Theme.of(context).textTheme.caption),
                  ],
                ),
              ),
              Visibility(
                visible: shouldDisplayPreferences,
                child: Icon(
                  shouldDisplayNowDetailWind ? Icons.check_box : Icons.check_box_outline_blank,
                  size: checkBoxSize,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
          onPressed: () {
            if (shouldDisplayPreferences) {
              setState(() {
                shouldDisplayNowDetailWind = !shouldDisplayNowDetailWind;
                prefs.setBool(prefsKeyShouldDisplayNowDetailWind, shouldDisplayNowDetailWind);
              });
            } else {
              final _gust = _windGust == null ? '' : ', gusting to $_windGust';
              final units = widget.isCelsius ? 'meters per second' : 'miles per hour';
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    'Wind: from the ${widget.weatherNow.windDirectionFrom} at $_windSpeed $units$_gust',
                  ),
                ),
              );
            }
          },
        ),
      );
    }
    if (shouldDisplayPreferences || shouldDisplayNowDetailSunrise) {
      nowDetailsWidgets.add(
        CupertinoButton(
          padding: EdgeInsets.zero,
          child: Column(
            children: <Widget>[
              Opacity(
                opacity: iconOpacityNowDetails,
                child: SvgPicture.asset(
                  assetNameIconSunrise,
                  color: Theme.of(context).primaryColorDark,
                  height: pictureSizeNowDetails,
                  width: pictureSizeNowDetails,
                ),
              ),
              SizedBox(height: 8),
              Visibility(
                visible: !shouldDisplayPreferences,
                child: Text(getTime(widget.weatherNow.sunriseLocal, widget.is24HourClock), style: textStyleNowDetails),
              ),
              Visibility(
                visible: shouldDisplayPreferences,
                child: Icon(
                  shouldDisplayNowDetailSunrise ? Icons.check_box : Icons.check_box_outline_blank,
                  size: checkBoxSize,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
          onPressed: () {
            if (shouldDisplayPreferences) {
              setState(() {
                shouldDisplayNowDetailSunrise = !shouldDisplayNowDetailSunrise;
                prefs.setBool(prefsKeyShouldDisplayNowDetailSunrise, shouldDisplayNowDetailSunrise);
              });
            } else {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Sunrise: ${getTime(widget.weatherNow.sunriseLocal, widget.is24HourClock)}')),
              );
            }
          },
        ),
      );
    }
    if (shouldDisplayPreferences || shouldDisplayNowDetailSunset) {
      nowDetailsWidgets.add(
        CupertinoButton(
          padding: EdgeInsets.zero,
          child: Column(
            children: <Widget>[
              Opacity(
                opacity: iconOpacityNowDetails,
                child: SvgPicture.asset(
                  assetNameIconSunset,
                  color: Theme.of(context).primaryColorDark,
                  height: pictureSizeNowDetails,
                  width: pictureSizeNowDetails,
                ),
              ),
              SizedBox(height: 8),
              Visibility(
                visible: !shouldDisplayPreferences,
                child: Text(getTime(widget.weatherNow.sunsetLocal, widget.is24HourClock), style: textStyleNowDetails),
              ),
              Visibility(
                visible: shouldDisplayPreferences,
                child: Icon(
                  shouldDisplayNowDetailSunset ? Icons.check_box : Icons.check_box_outline_blank,
                  size: checkBoxSize,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
          onPressed: () {
            if (shouldDisplayPreferences) {
              setState(() {
                shouldDisplayNowDetailSunset = !shouldDisplayNowDetailSunset;
                prefs.setBool(prefsKeyShouldDisplayNowDetailSunset, shouldDisplayNowDetailSunset);
              });
            } else {
              ScaffoldMessenger.of(context).removeCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Sunset: ${getTime(widget.weatherNow.sunsetLocal, widget.is24HourClock)}')),
              );
            }
          },
        ),
      );
    }
    nowDetailsWidgets.add(
      Column(
        children: <Widget>[
          IconButton(
            padding: EdgeInsets.all(4),
            icon: Opacity(
              opacity: shouldDisplayPreferences ? 1.0 : iconOpacityNowDetails,
              child: Icon(
                shouldDisplayPreferences ? Icons.done : Icons.edit,
                size: shouldDisplayPreferences ? 32 : 24,
                color: shouldDisplayPreferences ? Theme.of(context).accentColor : Theme.of(context).primaryColorDark,
              ),
            ),
            onPressed: () {
              setState(() {
                shouldDisplayPreferences = !shouldDisplayPreferences;
              });
            },
          ),
        ],
      ),
    );

    final detailsRow = Padding(
      padding: EdgeInsets.fromLTRB(mainScreenHorizontalPadding, 0, mainScreenHorizontalPadding, 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: nowDetailsWidgets,
      ),
    );

    if (shouldDisplayNowDetails) {
      listItems.add(detailsRow);
    }

    final toggleVisibilityOfDetails = () {
      final _duration = Duration(milliseconds: 200);
      if (listItems.isEmpty) {
        listItems.add(detailsRow);
        _listKey.currentState.insertItem(0, duration: _duration);
      } else {
        listItems.clear();
        _listKey.currentState.removeItem(
          0,
          (context, animation) => _buildItem(context, detailsRow, animation),
          duration: _duration,
        );
      }
    };

    return Column(
      children: [
        _ToggleArrowButton(shouldDisplayNowDetails, toggleVisibilityOfDetails),
        AnimatedList(
          key: _listKey,
          initialItemCount: listItems.length,
          shrinkWrap: true,
          primary: false,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (BuildContext context, int index, Animation<double> animation) {
            return _buildItem(context, detailsRow, animation);
          },
        ),
      ],
    );
  }

  Widget _buildItem(BuildContext context, Widget item, Animation<double> animation) {
    return SizeTransition(
      sizeFactor: animation,
      axisAlignment: -1,
      child: FadeTransition(
        opacity: animation,
        child: item,
      ),
    );
  }
}

class _ToggleArrowButton extends StatefulWidget {
  final bool isUp;
  final callback;

  _ToggleArrowButton(this.isUp, this.callback);

  @override
  State<StatefulWidget> createState() {
    return _ToggleArrowButtonState();
  }
}

class _ToggleArrowButtonState extends State<_ToggleArrowButton> {
  bool _isUp;

  @override
  void initState() {
    super.initState();
    _isUp = widget.isUp;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: CupertinoButton(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Icon(
              _isUp ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
              color: Theme.of(context).primaryColorDark.withAlpha((0.8 * 255).round()),
            ),
            onPressed: () async {
              setState(() => _isUp = !_isUp);
              widget.callback();
              prefs.setBool(prefsKeyShouldDisplayNowDetails, _isUp);
            },
          ),
        ),
      ],
    );
  }
}
