String getCountry(String code) {
  if (code == 'US') {
    return 'United States';
  } else if (code == 'AL') {
    return 'Albania';
  } else if (code == 'AM') {
    return 'Armenia';
  } else if (code == 'AO') {
    return 'Angola';
  } else if (code == 'AR') {
    return 'Argentina';
  } else if (code == 'AT') {
    return 'Austria';
  } else if (code == 'AU') {
    return 'Australia';
  } else if (code == 'AW') {
    return 'Aruba';
  } else if (code == 'AZ') {
    return 'Azerbaijan';
  } else if (code == 'BA') {
    return 'Boznia & Herzegovina';
  } else if (code == 'BE') {
    return 'Belgium';
  } else if (code == 'BG') {
    return 'Bulgaria';
  } else if (code == 'BJ') {
    return 'Benin';
  } else if (code == 'BR') {
    return 'Brazil';
  } else if (code == 'BY') {
    return 'Belarus';
  } else if (code == 'CA') {
    return 'Canada';
  } else if (code == 'CD') {
    return 'D.R. Congo';
  } else if (code == 'CF') {
    return 'Central African Republic';
  } else if (code == 'CH') {
    return 'Switzerland';
  } else if (code == 'CW') {
    return 'Curaçao';
  } else if (code == 'CY') {
    return 'Cyprus';
  } else if (code == 'CZ') {
    return 'Czech Republic';
  } else if (code == 'DE') {
    return 'Germany';
  } else if (code == 'DJ') {
    return 'Djibouti';
  } else if (code == 'DK') {
    return 'Denmark';
  } else if (code == 'DZ') {
    return 'Algeria';
  } else if (code == 'EE') {
    return 'Estonia';
  } else if (code == 'EG') {
    return 'Egypt';
  } else if (code == 'EH') {
    return 'Western Sahara';
  } else if (code == 'ES') {
    return 'Spain';
  } else if (code == 'ET') {
    return 'Ethiopia';
  } else if (code == 'ER') {
    return 'Eritrea';
  } else if (code == 'FI') {
    return 'Finland';
  } else if (code == 'FM') {
    return 'Micronesia';
  } else if (code == 'FR') {
    return 'France';
  } else if (code == 'GB') {
    return 'United Kingdom';
  } else if (code == 'GE') {
    return 'Georgia';
  } else if (code == 'GF') {
    return 'French Guiana';
  } else if (code == 'GG') {
    return 'Guernsey';
  } else if (code == 'GQ') {
    return 'Equatorial Guinea';
  } else if (code == 'GR') {
    return 'Greece';
  } else if (code == 'GW') {
    return 'Guinea-Bissau';
  } else if (code == 'HR') {
    return 'Croatia';
  } else if (code == 'HU') {
    return 'Hungary';
  } else if (code == 'ID') {
    return 'Indonesia';
  } else if (code == 'IE') {
    return 'Ireland';
  } else if (code == 'IL') {
    return 'Occupied Palestine';
  } else if (code == 'IN') {
    return 'India';
  } else if (code == 'IQ') {
    return 'Iraq';
  } else if (code == 'IS') {
    return 'Iceland';
  } else if (code == 'IT') {
    return 'Italy';
  } else if (code == 'IR') {
    return 'Iran';
  } else if (code == 'JE') {
    return 'Jersey';
  } else if (code == 'JO') {
    return 'Jordan';
  } else if (code == 'JP') {
    return 'Japan';
  } else if (code == 'KE') {
    return 'Kenya';
  } else if (code == 'KH') {
    return 'Cambodia';
  } else if (code == 'KP') {
    return 'North Korea';
  } else if (code == 'KR') {
    return 'South Korea';
  } else if (code == 'KY') {
    return 'Cayman Islands';
  } else if (code == 'KZ') {
    return 'Kazakhstan';
  } else if (code == 'LB') {
    return 'Lebanon';
  } else if (code == 'LK') {
    return 'Sri Lanka';
  } else if (code == 'LT') {
    return 'Lithuania';
  } else if (code == 'LV') {
    return 'Latvia';
  } else if (code == 'LY') {
    return 'Lybia';
  } else if (code == 'MA') {
    return 'Morocco';
  } else if (code == 'MD') {
    return 'Moldova';
  } else if (code == 'ME') {
    return 'Montenegro';
  } else if (code == 'MF') {
    return 'Saint Martin';
  } else if (code == 'MH') {
    return 'Marshall Islands';
  } else if (code == 'MK') {
    return 'Macedonia';
  } else if (code == 'MT') {
    return 'Malta';
  } else if (code == 'MX') {
    return 'Mexico';
  } else if (code == 'NA') {
    return 'Namibia';
  } else if (code == 'NL') {
    return 'Netherlands';
  } else if (code == 'NO') {
    return 'Norway';
  } else if (code == 'NZ') {
    return 'New Zealand';
  } else if (code == 'PH') {
    return 'Philippines';
  } else if (code == 'PL') {
    return 'Poland';
  } else if (code == 'PS') {
    return 'Palestine';
  } else if (code == 'PT') {
    return 'Portugal';
  } else if (code == 'PW') {
    return 'Palau';
  } else if (code == 'RO') {
    return 'Romania';
  } else if (code == 'RU') {
    return 'Russia';
  } else if (code == 'RS') {
    return 'Serbia';
  } else if (code == 'RW') {
    return 'Rwanda';
  } else if (code == 'SA') {
    return 'Saudi Arabia';
  } else if (code == 'SB') {
    return 'Solomon Islands';
  } else if (code == 'SD') {
    return 'Sudan';
  } else if (code == 'SE') {
    return 'Sweden';
  } else if (code == 'SI') {
    return 'Slovenia';
  } else if (code == 'SK') {
    return 'Slovakia';
  } else if (code == 'SO') {
    return 'Somalia';
  } else if (code == 'SS') {
    return 'South Sudan';
  } else if (code == 'SY') {
    return 'Syria';
  } else if (code == 'TD') {
    return 'Chad';
  } else if (code == 'TM') {
    return 'Turkmenistan';
  } else if (code == 'TR') {
    return 'Turkey';
  } else if (code == 'TZ') {
    return 'Tanzania';
  } else if (code == 'UA') {
    return 'Ukraine';
  } else if (code == 'UG') {
    return 'Uganda';
  } else if (code == 'UZ') {
    return 'Uzbekistan';
  } else if (code == 'VG') {
    return 'Virgin Islands';
  } else if (code == 'WS') {
    return 'Samoa';
  } else if (code == 'XK') {
    return 'Kosovo';
  } else if (code == 'YE') {
    return 'Yemen';
  } else if (code == 'ZA') {
    return 'South Africa';
  } else if (code == 'ZM') {
    return 'Zambia';
  } else if (code == 'ZW') {
    return 'Zimbabwe';
  } else {
    return code;
  }
}
