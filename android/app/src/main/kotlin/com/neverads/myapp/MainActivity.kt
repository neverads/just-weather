package com.neverads.myapp

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.widget.RemoteViews
import com.neverads.just_weather.AtAGlanceWidget
import com.neverads.just_weather.getImageId
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity : FlutterActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GeneratedPluginRegistrant.registerWith(this)
        MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "setIsCelsiusAndLocationId") {
                try {
                    setIsCelsiusAndLocationId(call.argument("isCelsius")!!, call.argument("locationId")!!)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("ERROR", "Error attempting to 'put' to SharedPreferences.", null)
                }
            } else if (call.method == "updateWidgetWithoutAnotherFetch") {
                try {
                    updateWidgetWithoutAnotherFetch(
                            this,
                            call.argument("currentTemp")!!,
                            call.argument("highTemp")!!,
                            call.argument("lowTemp")!!,
                            call.argument("weatherIconCode")!!,
                            call.argument("cloudinessPercentage")!!)
                    result.success(true)
                } catch (e: Exception) {
                    result.error("ERROR", "Error updating widget.", null)
                }
            } else {
                result.notImplemented()
            }
        }
    }

    private fun setIsCelsiusAndLocationId(isCelsius: Boolean, locationId: String) {
        return getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(PREFS_KEY_IS_CELSIUS, isCelsius)
                .putString(PREFS_KEY_LOCATION_ID, locationId)
                .apply()
    }

    private fun updateWidgetWithoutAnotherFetch(context: Context, currentTemp: Int, highTemp: Int, lowTemp: Int, weatherIconCode: String,
                                                cloudinessPercentage: Int) {
        val appWidgetManager = AppWidgetManager.getInstance(application)
        val appWidgetIds: IntArray = appWidgetManager
                .getAppWidgetIds(ComponentName(context.applicationContext, AtAGlanceWidget::class.java))
        for (appWidgetId in appWidgetIds) {
            val views = RemoteViews(context.packageName, R.layout.at_a_glance_widget)
            views.setTextViewText(R.id.appwidget_temp_now, "$currentTemp˚")
            views.setTextViewText(R.id.appwidget_temp_daily_high_low, "↑ $highTemp˚ / $lowTemp˚ ↓")
            views.setImageViewResource(R.id.appwidget_weather_icon, getImageId(weatherIconCode, cloudinessPercentage))
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}
