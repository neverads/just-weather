package com.neverads.myapp

const val PREFERENCES = "preferences"
const val PREFS_KEY_LOCATION_ID = "prefs_key_location_id"
const val PREFS_KEY_IS_CELSIUS = "prefs_key_is_celsius"
const val CHANNEL = "just_weather.never_ads/location"
