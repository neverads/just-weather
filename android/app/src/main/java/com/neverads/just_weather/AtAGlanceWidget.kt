package com.neverads.just_weather

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.view.View
import android.widget.RemoteViews
import com.neverads.myapp.*
import org.json.JSONObject
import java.net.URL
import java.util.*
import kotlin.math.roundToInt

/**
 * Implementation of App Widget functionality.
 */

var lastRefresh = 0L

class AtAGlanceWidget : AppWidgetProvider() {
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            val views = RemoteViews(context.packageName, R.layout.at_a_glance_widget)
            views.setViewVisibility(R.id.appwidget_temp_now, View.INVISIBLE)
            views.setViewVisibility(R.id.appwidget_temp_daily_high_low, View.INVISIBLE)
            views.setViewVisibility(R.id.appwidget_weather_icon, View.INVISIBLE)
            appWidgetManager.updateAppWidget(appWidgetId, views)
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {}
    override fun onDisabled(context: Context) {}
}

internal fun updateAppWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
    AsyncTask.execute {
        val views = RemoteViews(context.packageName, R.layout.at_a_glance_widget)
        val nowMillis = System.currentTimeMillis()
        val tenMinInMillis = 600_000
        if (lastRefresh == 0L || nowMillis - lastRefresh > tenMinInMillis) {
            Thread.sleep(200)
            lastRefresh = nowMillis
            val kelvinCelsiusDifference = 273.15
            val placeholder = "--"
            var currentTempAsString = placeholder
            var currentIconCode = "a fake code will be 'else' and show clouds"
            var cloudinessPercentage = 100
            var highTempAsString = placeholder
            var lowTempAsString = placeholder

            try {
                val prefs = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
                val locationId = prefs.getString(PREFS_KEY_LOCATION_ID, "")!!
                if (locationId.isNotBlank()) {
                    val isCelsius = prefs.getBoolean(PREFS_KEY_IS_CELSIUS, true)
                    val split = locationId.split("___")
                    val lat = split.first()
                    val long = split[1]

                    val jsonAsString = URL("https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$long&exclude=minutely&appid=$OWM_API_KEY").readText()
                    val json = JSONObject(jsonAsString)
                    val current = json.getJSONObject("current")
                    val currentTempInCelsius = (current.getDouble("temp") - kelvinCelsiusDifference).roundToInt()
                    currentIconCode = current.getJSONArray("weather").getJSONObject(0).getString("icon")
                    cloudinessPercentage = current.getInt("clouds")
                    val dailyForecastToday = json.getJSONArray("daily").getJSONObject(0)
                    val dailyTemps = dailyForecastToday.getJSONObject("temp")
                    var highTempInCelsius = (dailyTemps.getDouble("max") - kelvinCelsiusDifference).roundToInt()
                    var lowTempInCelsius = (dailyTemps.getDouble("min") - kelvinCelsiusDifference).roundToInt()
                    highTempInCelsius = listOf(highTempInCelsius, currentTempInCelsius).max()!!
                    lowTempInCelsius = listOf(lowTempInCelsius, currentTempInCelsius).min()!!

                    val dayTodayFromForecast = Date(dailyForecastToday.getInt("dt").toLong() * 1000).day
                    if (dayTodayFromForecast != Date().day) {
                        val hourlyTemps = mutableListOf(currentTempInCelsius)
                        val hourlyDataPoints = json.getJSONArray("hourly")
                        repeat(18) { index ->  // don't override today's high/low with tomorrow's
                            val tempKelvin = hourlyDataPoints.getJSONObject(index).getDouble("temp")
                            hourlyTemps.add((tempKelvin - kelvinCelsiusDifference).roundToInt())
                        }
                        highTempInCelsius = hourlyTemps.max()!!
                        lowTempInCelsius = hourlyTemps.min()!!
                    }

                    if (isCelsius) {
                        currentTempAsString = currentTempInCelsius.toString()
                        highTempAsString = highTempInCelsius.toString()
                        lowTempAsString = lowTempInCelsius.toString()
                    } else {
                        currentTempAsString = celsiusToFahrenheit(currentTempInCelsius).toString()
                        highTempAsString = celsiusToFahrenheit(highTempInCelsius).toString()
                        lowTempAsString = celsiusToFahrenheit(lowTempInCelsius).toString()
                    }
                    currentTempAsString += '˚'
                    highTempAsString += '˚'
                    lowTempAsString += '˚'
                }

            } catch (error: Exception) {
                println("ERROR: cannot update widget - likely network connection failure")
            }
            views.setTextViewText(R.id.appwidget_temp_now, currentTempAsString)
            views.setTextViewText(R.id.appwidget_temp_daily_high_low, "↑ $highTempAsString / $lowTempAsString ↓")
            views.setImageViewResource(R.id.appwidget_weather_icon, getImageId(currentIconCode, cloudinessPercentage))
        } else {
            Thread.sleep(400)
        }
        views.setViewVisibility(R.id.appwidget_temp_now, View.VISIBLE)
        views.setViewVisibility(R.id.appwidget_temp_daily_high_low, View.VISIBLE)
        views.setViewVisibility(R.id.appwidget_weather_icon, View.VISIBLE)

        //Create an Intent with the AppWidgetManager.ACTION_APPWIDGET_UPDATE action//
        val intentUpdate = Intent(context, AtAGlanceWidget::class.java)
        intentUpdate.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        //Update the current widget instance only, by creating an array that contains the widget’s unique ID//
        val idArray = intArrayOf(appWidgetId)
        intentUpdate.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, idArray)
        //Wrap the intent as a PendingIntent, using PendingIntent.getBroadcast()//
        val pendingUpdate = PendingIntent.getBroadcast(context, appWidgetId, intentUpdate, PendingIntent.FLAG_UPDATE_CURRENT)
        //Send the pending intent in response to the user tapping the ‘Update’ TextView//
        views.setOnClickPendingIntent(R.id.appwidget_refresh_tap_area, pendingUpdate)

        views.setOnClickPendingIntent(R.id.widgetRoot,
                PendingIntent.getActivity(context, 0, Intent(context, MainActivity::class.java), 0))

        appWidgetManager.updateAppWidget(appWidgetId, views)
    }
}

internal fun getImageId(code: String, cloudinessPercentage: Int): Int {
    if (code in listOf("01d", "02d", "03d", "04d")) return when {
        cloudinessPercentage <= 10 -> R.drawable.ic_sun
        cloudinessPercentage <= 50 -> R.drawable.ic_light_cloud_sun
        cloudinessPercentage <= 95 -> R.drawable.ic_heavy_cloud_sun
        else -> R.drawable.ic_cloud
    }
    else if (code in listOf("01n", "02n", "03n", "04n")) return when {
        cloudinessPercentage <= 10 -> R.drawable.ic_moon_and_stars
        cloudinessPercentage <= 50 -> R.drawable.ic_light_cloud_moon
        cloudinessPercentage <= 95 -> R.drawable.ic_heavy_cloud_moon
        else -> R.drawable.ic_cloud
    }
    else return when (code) {
        in listOf("09d", "09n") -> R.drawable.ic_showers
        "10d" -> R.drawable.ic_cloud_sun_rain
        "10n" -> R.drawable.ic_cloud_moon_rain
        in listOf("11d", "11n") -> R.drawable.ic_thunderstorm
        in listOf("13d", "13n") -> R.drawable.ic_snowflake
        else -> R.drawable.ic_cloud
    }
}

internal fun celsiusToFahrenheit(degreesCelsius: Int): Int {
    return (degreesCelsius * 9.0 / 5 + 32).roundToInt()
}
