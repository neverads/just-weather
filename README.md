# just weather
by [never ads](https://play.google.com/store/apps/developer?id=Never+Ads)

_No ads, no data-collection, no cost.  Ever.  And we're [open-source](gitlab.com/neverads)!_  

Weather forecast - that's it.  

Do you want an app which doesn't take up a ton of space on your device?  
Do you hate ads and unnecessary graphical fluff?  

You're welcome. 😘  

### source for weather data  
We use [Open Weather](https://openweathermap.org) as our data-source.
